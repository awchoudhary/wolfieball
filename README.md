# README #


### What is this repository for? ###

* WolfieBall is a fantasy baseball draft kit application that allows users to draft fantasy baseball teams according to player statistics. 
* The application was made using Java. The user-interface was made using JavaFX

### How do I get set up? ###
* Pull and run WolfieBall/src/wb/WolfieBall.java