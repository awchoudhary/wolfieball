package wb.controller;

import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wb.WB_PropertyType;
import static wb.WB_PropertyType.REMOVE_PLAYER_MESSAGE;
import static wb.WB_PropertyType.REMOVE_TEAM_MESSAGE;
import wb.data.Draft;
import wb.data.DraftDataManager;
import wb.data.FantasyTeam;
import wb.data.Hitter;
import wb.data.Pitcher;
import wb.data.Player;
import wb.gui.AddPlayerDialog;
import wb.gui.EditPlayerDialog;
import wb.gui.MessageDialog;
import wb.gui.PlayersScreenGui;
import wb.gui.SwitchScreenGui;
import wb.gui.YesNoCancelDialog;

/**
 *
 * @author Awaes
 */
public class PlayerEditController {
    EditPlayerDialog epd;
    AddPlayerDialog apd;
    YesNoCancelDialog yesNoCancelDialog;
    MessageDialog messageDialog;
    
    public PlayerEditController(Stage initPrimaryStage, YesNoCancelDialog initYesNoCancelDialog, MessageDialog initMessageDialog){
        epd = new EditPlayerDialog(initPrimaryStage, messageDialog);
        apd = new AddPlayerDialog(initPrimaryStage);
        yesNoCancelDialog = initYesNoCancelDialog;
        messageDialog = initMessageDialog;
    }
    
    public void handleAddPlayerRequest(SwitchScreenGui gui, PlayersScreenGui playersScreenGui){
        DraftDataManager ddm = gui.getDataManager();
        Draft draft = ddm.getDraft();
        apd.showAddPlayerDialog();
        // DID THE USER CONFIRM?
        if (apd.wasCompleteSelected()) {
            
            // GET THE PLAYER   
            if(apd.isCheckBoxPSelected()){
                Pitcher p = new Pitcher();
                p = apd.getPitcher();
                playersScreenGui.getPlayersList().add(p);
                playersScreenGui.getVisiblePlayersList().add(p);
                playersScreenGui.getReferenceList().add(p);

            }
            else{
                String QP = new String();
                Hitter h = new Hitter();
                h = apd.getHitter();
                //BUILD THE QP STRING FOR HITTER
                if(apd.isCheckBoxCSelected()){
                    QP = QP + "C_";
                }
                if(apd.isCheckBox1BSelected()){
                    QP = QP + "1B_";
                }
                if(apd.isCheckBox3BSelected()){
                    QP = QP + "3B_";
                }
                if(apd.isCheckBox2BSelected()){
                    QP = QP + "2B_";
                }
                if(apd.isCheckBoxSSSelected()){
                    QP = QP + "SS_";
                }
                if(apd.isCheckBoxOFSelected()){
                    QP = QP + "OF_";
                }
                //REMOVE ENDING _ FROM STRING
                QP = QP.substring(0, QP.length() - 1);
                h.setQP(QP);
                playersScreenGui.getPlayersList().add(h);
                playersScreenGui.getVisiblePlayersList().add(h);
                playersScreenGui.getReferenceList().add(h);
            }
            
            
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }

    }
    
    public void handleRemovePlayerRequest(PlayersScreenGui gui, Player playerToRemove){
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_PLAYER_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();
        
        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getPlayersList().remove(playerToRemove);
            gui.getVisiblePlayersList().remove(getIndexOfPlayer(gui.getVisiblePlayersList(), playerToRemove));
            if(!gui.getReferenceList().isEmpty())
            gui.getReferenceList().remove(getIndexOfPlayer(gui.getReferenceList(), playerToRemove));
        }

    }
    
    public void handleEditPlayerRequest(SwitchScreenGui gui, PlayersScreenGui playersScreenGui, Player playerToEdit){
        DraftDataManager ddm = gui.getDataManager();
        Draft draft = ddm.getDraft();
        String previousTeamName = playerToEdit.getFantasyTeamName();
        epd.showEditPlayerDialog(draft, playerToEdit);
        HashMap<String, Player> draftedPlayerMappings = draft.getDraftedPlayersMappings();
        
        // DID THE USER CONFIRM?
        if (epd.wasCompleteSelected()){
            Player p = epd.getPlayer();
            //update player
            p.clone(playerToEdit);
            //move player if assigned to a team
            //IF PLAYER WAS EDITED WITHIN THE SAME TEAM
            if(playerToEdit.getFantasyTeamName().equals(previousTeamName)){
                if(playerToEdit.getContract().equals("S1")){
                    draft.removeFromDraftedPlayers(playerToEdit);
                }
                if(playerToEdit.getContract().equals("S2")){
                    if(!draft.getDraftedPlayers().contains(playerToEdit)){
                        draft.addToDraftedPlayers(playerToEdit);
                    }
                }
            }
            //IF AN INTER TEAM CHANGE OF PLAYERS OCCURED 
            else if(!playerToEdit.getFantasyTeamName().equals("Free Agent") && !previousTeamName.equals("Free Agent")){
                handleTeamChangeRequest(playerToEdit, previousTeamName, draft);
                //change name of fantasy team for the player in the drafted players array of the draft
                draftedPlayerMappings.get(playerToEdit.getFirstName()).setFantasyTeamName(playerToEdit.getFantasyTeamName());
                draftedPlayerMappings.get(playerToEdit.getFirstName()).setDraftNumber(playerToEdit.getDraftNumber());
            }
            //if a player is assigned to a team from free agent pool
            else if(!playerToEdit.getFantasyTeamName().equals("Free Agent")){
                playerToEdit.setFantasyTeamOwner(draft.getFantasyTeamMappings().get(playerToEdit.getFantasyTeamName()).getOwnerName());
                playersScreenGui.getVisiblePlayersList().remove(playersScreenGui.getVisiblePlayersList().get(getIndexOfPlayer(playersScreenGui.getVisiblePlayersList(), playerToEdit)));
                if(!playersScreenGui.getReferenceList().isEmpty()){
                    playersScreenGui.getReferenceList().remove(playersScreenGui.getReferenceList().get(getIndexOfPlayer(playersScreenGui.getReferenceList(), playerToEdit)));
                }
                draft.getFantasyTeamMappings().get(playerToEdit.getFantasyTeamName()).addToVisibleStartingLineUp(playerToEdit);
                draft.addToDraftedPlayers(playerToEdit);
                draft.calculateAndAssignTotalPoints();
            }
            //if player is removed from a team to free agent pool
            else if(playerToEdit.getFantasyTeamName().equals("Free Agent") && !previousTeamName.equals("Free Agent")){
                playerToEdit.setFantasyTeamOwner("");
                draft.getFantasyTeamMappings().get(previousTeamName).removeFromVisibleStartingLineUp(playerToEdit);
                playerToEdit.clone(draft.getPlayersList().get(getIndexOfPlayer(draft.getPlayersList(), playerToEdit)));
                draft.removeFromDraftedPlayers(playerToEdit);
                draft.calculateAndAssignTotalPoints();
            }
            //the player must not have been assigned to a team
            else{
            //do nothing
            }
        }
        else{
            //cancel must have been selected
        }
    }
    
    public void handleTeamChangeRequest(Player playerToEdit, String previousTeamName, Draft draft){
        HashMap<String, FantasyTeam> fantasyTeamsMap = draft.getFantasyTeamMappings();
        playerToEdit.setFantasyTeamOwner(fantasyTeamsMap.get(playerToEdit.getFantasyTeamName()).getOwnerName());
        fantasyTeamsMap.get(playerToEdit.getFantasyTeamName()).addToVisibleStartingLineUp(playerToEdit);
        fantasyTeamsMap.get(previousTeamName).removeFromVisibleStartingLineUp(playerToEdit);
        playerToEdit.clone(draft.getPlayersList().get(getIndexOfPlayer(draft.getPlayersList(), playerToEdit)));
    }
    
    private int getIndexOfPlayer(ObservableList<Player> list, Player p){
        for(int i = 0; i < list.size(); i++){
            if(p.equals(list.get(i))){
                return i;
            }
        }
        return -1;
    }
    
   

    
}
