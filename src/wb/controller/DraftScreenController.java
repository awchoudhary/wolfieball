package wb.controller;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import wb.data.Draft;
import wb.data.DraftDataManager;
import wb.data.FantasyTeam;
import wb.data.Hitter;
import wb.data.Pitcher;
import wb.data.Player;

/**
 *
 * @author Awaes
 */
public class DraftScreenController {
    //DATA MANAGER USED BY CLASS
    DraftDataManager dataManager;
    Thread thread;
    boolean isSuspended = false;
    
    public DraftScreenController(DraftDataManager initDataManager){
        dataManager = initDataManager;
    }
    
    //ADDS AN ELIGIBLE PLAYER TO A FANTASY TEAM
    public void handleDraftPlayerRequest(){
        Draft draft = dataManager.getDraft();
        FantasyTeam team = new FantasyTeam();
        //GET A FANTASY TEAM
        team = findFantasyTeam();
        if(team == null){
            return;
        }
        //GET A PLAYER
        Player p = findPlayer(team);
        //ADD PLAYER TO TEAM
        draft.getFantasyTeamMappings().get(team.getName()).addToVisibleStartingLineUp(p);
        draft.addToDraftedPlayers(p);
        draft.calculateAndAssignTotalPoints();
    }
    
    public void handleStartAutomatedDraftRequest(){
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                Draft draft = dataManager.getDraft();
                int playersNeeded = getTotalPlayersNeeded();
                for (int i = 0; i < playersNeeded; i++) {
                        //final FantasyTeam team = findFantasyTeam();
                        //GET A FANTASY TEAM
                        //GET A PLAYER
                        //final Player p = findPlayer(team);

                        // WE'RE SLEEPING FIRST JUST TO LET THE FIRST MESSAGE SHOW
                        Thread.sleep(1000);
                        
                        // UPDATE ANY PROGRESS DISPLAY
                        Platform.runLater(new Runnable() {
                            public void run() {
                                FantasyTeam team = findFantasyTeam();
                                Player p = findPlayer(team);
                                //ADD PLAYER TO TEAM
                                draft.getFantasyTeamMappings().get(team.getName()).addToVisibleStartingLineUpForAutomatedDraft(p);
                                draft.addToDraftedPlayers(p);
                                draft.calculateAndAssignTotalPoints();
                            }
                        });
                        //System.out.println("EXPORTING " + pages[pageIndex]);
                    };
                
                return null;
            }
        };
        if(isSuspended){
            thread.resume();
        }
        else{
            thread = new Thread(task);
            thread.start();
        }
    }
    
    public void handlePauseAutomatedDraft(){
        thread.suspend();
        isSuspended = true;
    }
    
    private FantasyTeam findFantasyTeam(){
        Draft draft = dataManager.getDraft();
        //RETURN TEAM THAT DOES NOT HAVE A FULL STARTING LINE UP YET
        for(int i = 0; i < draft.getTeams().size(); i++){
            if(draft.getTeams().get(i).getVisibleStartingLineUp().size() < 23){
                return draft.getTeams().get(i);
            }
        }
        //ELSE RETURN TEAM THAT DOES NOT HAVE FULL TAXI DRAFT YET
        for(int i = 0; i < draft.getTeams().size(); i++){
            if(draft.getTeams().get(i).getTaxiSquad().size() < 9){
                return draft.getTeams().get(i);
            }
        }
        //ELSE RETURN NULL
       return null;
    }
    
    //METHOD TO FIND PLAYER TO DRAFT
    private Player findPlayer(FantasyTeam team){
        Draft draft = dataManager.getDraft();
        ObservableList<Player> playersList = draft.getPlayersList();
        if(team.getCountC() < 2){
            return findPlayerWithPosition("C", playersList, team);
        }
        if(team.getCount1B() < 1){
            return findPlayerWithPosition("1B", playersList, team);
        }
        if(team.getCountCI() < 1){
            return findPlayerWithPosition("CI", playersList, team);
        }
        if(team.getCount3B() < 1){
            return findPlayerWithPosition("3B", playersList, team);
        }
        if(team.getCount2B() < 1){
            return findPlayerWithPosition("2B", playersList, team);
        }
        if(team.getCountMI() < 1){
            return findPlayerWithPosition("MI", playersList, team);
        }
        if(team.getCountSS() < 1){
            return findPlayerWithPosition("SS", playersList, team);
        }
        if(team.getCountOF() < 5){
            return findPlayerWithPosition("OF", playersList, team);
        }
        if(team.getCountU() < 1){
            return findPlayerWithPosition("U", playersList, team);
        }
        if(team.getCountP() < 9){
            return findPlayerWithPosition("P", playersList, team);
        }
        //THIS MUST MEAN THAT STARTING LINE UP FOR TEAM IS FULL
        return findAnyPlayer(playersList, team);
    }
    
    private Player findPlayerWithPosition(String position, ObservableList<Player> list, FantasyTeam team){
        for(int i = 0; i < list.size(); i++){
            if(position.equals("P")){
                if(list.get(i) instanceof Pitcher && list.get(i).getFantasyTeamName().equals("Free Agent")){
                    list.get(i).setFantasyTeamName(team.getName());
                    list.get(i).setPosition("P");
                    list.get(i).setContract("S2");
                    list.get(i).setSalary(1);
                    return list.get(i);
                }
            }
            else if(position.equals("C")){
                if(((Hitter)list.get(i)).getQP().contains("C_") && list.get(i).getFantasyTeamName().equals("Free Agent")){
                    list.get(i).setFantasyTeamName(team.getName());
                    list.get(i).setPosition("C");
                    list.get(i).setContract("S2");
                    list.get(i).setSalary(1);
                    return list.get(i);
                }
            }
            else{
                if(((Hitter)list.get(i)).getQP().contains(position) && list.get(i).getFantasyTeamName().equals("Free Agent")){
                    list.get(i).setFantasyTeamName(team.getName());
                    list.get(i).setPosition(position);
                    list.get(i).setContract("S2");
                    list.get(i).setSalary(1);
                    return list.get(i);
                }
            }
        }
        return null;
    }
    
    private Player findAnyPlayer(ObservableList<Player> list, FantasyTeam team){
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).getFantasyTeamName().equals("Free Agent")){
                list.get(i).setFantasyTeamName(team.getName());
                list.get(i).setContract("S2");
                list.get(i).setSalary(1);
                return list.get(i);
            }
        }
        return null;
    }
    
    private int getTotalPlayersNeeded(){
        int totalPlayersNeeded = 0;
        Draft draft = dataManager.getDraft();
        for(int i = 0; i < draft.getTeams().size(); i++){
            totalPlayersNeeded += draft.getTeams().get(i).getHittersNeeded();
            totalPlayersNeeded += draft.getTeams().get(i).getPitchersNeeded();
            totalPlayersNeeded += draft.getTeams().get(i).getTaxiPlayersNeeded();
        }
        return totalPlayersNeeded;
    }
    
    
    
    
}
