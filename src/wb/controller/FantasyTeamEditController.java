package wb.controller;

import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wb.WB_PropertyType.REMOVE_TEAM_MESSAGE;
import wb.data.Draft;
import wb.data.DraftDataManager;
import wb.data.FantasyTeam;
import wb.data.Player;
import wb.gui.FantasyTeamsDialog;
import wb.gui.FantasyTeamsGui;
import wb.gui.SwitchScreenGui;
import wb.gui.YesNoCancelDialog;

/**
 *
 * @author Awaes
 */
public class FantasyTeamEditController {
    FantasyTeamsDialog ftd;
    YesNoCancelDialog yesNoCancelDialog;
    
    public FantasyTeamEditController(Stage initPrimaryStage, YesNoCancelDialog initYesNoCancelDialog) {
        ftd = new FantasyTeamsDialog(initPrimaryStage);
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR SCHEDULE ITEMS
    
    public void handleAddFantasyTeamRequest(SwitchScreenGui gui) {
        DraftDataManager ddm = gui.getDataManager();
        Draft draft = ddm.getDraft();
        ftd.showAddFantasyTeamDialog();
        
        // DID THE USER CONFIRM?
        if (ftd.wasCompleteSelected()) {
            // GET THE SCHEDULE ITEM
            FantasyTeam ft = ftd.getFantasyTeam();
            
            // AND ADD IT AS A ROW TO THE TABLE
            draft.addTeam(ft);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditFantasyTeamRequest(SwitchScreenGui gui, FantasyTeamsGui fantasyTeamsGui, FantasyTeam teamToEdit) {
        DraftDataManager ddm = gui.getDataManager();
        Draft draft = ddm.getDraft();
        ftd.showEditFantasyTeamDialog(teamToEdit);
        
        // DID THE USER CONFIRM?
        if (ftd.wasCompleteSelected()) {
            // UPDATE THE FANTAST TEAM
            FantasyTeam ft = ftd.getFantasyTeam();
            //update the name of team for the combobox 
            int index = gui.getDataManager().getDraft().getTeamNamesList().indexOf(teamToEdit.getName());
            gui.getDataManager().getDraft().getTeamNamesList().set(index, ft.getName());
            teamToEdit.setName(ft.getName());
            teamToEdit.setOwnerName(ft.getOwnerName());
            fantasyTeamsGui.getSelectTeamComboBox().setValue(ft.getName());
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveFantasyTeamRequest(SwitchScreenGui gui, FantasyTeam teamToRemove, FantasyTeamsGui fantasyTeamsGui, Boolean updatePlayersTable) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_TEAM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();
        
        Draft draft = gui.getDataManager().getDraft();
        ComboBox selectTeamComboBox = fantasyTeamsGui.getSelectTeamComboBox();
        ObservableList<String> teamNameList = draft.getTeamNamesList();
        
        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) {
            /*if(teamNameList.indexOf(teamToRemove.getName()) == 0){
                updatePlayersTable = false;
            }*/
            ObservableList<Player> visibleStartingLineUp = teamToRemove.getVisibleStartingLineUp();
            for(int i = 0; i < visibleStartingLineUp.size(); i++){
                visibleStartingLineUp.get(i).setFantasyTeamName("Free Agent");
                visibleStartingLineUp.get(i).clone(draft.getPlayersList().get(getIndexOfPlayer(draft.getPlayersList(), visibleStartingLineUp.get(i))));
            }
            //fantasyTeamsGui.getSelectTeamComboBox().setValue(draft.getTeamNamesList().get(draft.getTeamNamesList().indexOf(teamToRemove) - 1));
            draft.removeTeam(teamToRemove);
        }
    }
    
    private int getIndexOfPlayer(ObservableList<Player> list, Player p){
        for(int i = 0; i < list.size(); i++){
            if(p.equals(list.get(i))){
                return i;
            }
        }
        return -1;
    }


}
