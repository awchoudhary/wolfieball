package wb.file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import wb.data.Player;
import wb.data.Draft;
/**
 * This interface provides an abstraction of what a file manager should do. 
 * 
 * @author Awaes Choudhary
 */
public interface DraftFileManager {
    public void loadHitters(String hittersFilePath, Draft draftToLoadInto) throws IOException;
    public void loadPitchers(String pitchersFilePath, Draft draftToLoadInto) throws IOException;
    public void saveDraft(Draft draftToSave, String draftName) throws IOException;
    public void loadDraft(Draft draftToLoad, String JsonFilePath) throws IOException;
}
