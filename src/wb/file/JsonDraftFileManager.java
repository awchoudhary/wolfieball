package wb.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javafx.collections.ObservableList;
import wb.data.Player;
import wb.data.Draft;
import wb.data.Hitter;
import wb.data.Pitcher;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonValue;
import static wb.WB_StartupConstants.PATH_DRAFTS;
import wb.data.FantasyTeam;

/**
 * This is a DraftFileManager that uses the JSON file format to 
 * implement the necessary functions for loading and saving different
 * drafts and loading the players file.
 * 
 * @author Awaes Choudhary
 */
public class JsonDraftFileManager implements DraftFileManager {
    String JSON_PLAYER_TYPE = "PLAYER_TYPE";
    String JSON_HITTERS = "Hitters";
    String JSON_PITCHERS = "Pitchers";
    String JSON_TEAM = "TEAM";
    String JSON_TEAMS = "TEAMS";
    String JSON_PLAYERS = "PLAYERS";
    String JSON_TEAM_MLB = "TEAM_MLB";
    String JSON_POSITION = "POSITION";
    String JSON_LAST_NAME = "LAST_NAME";
    String JSON_FIRST_NAME = "FIRST_NAME";
    String JSON_FANTASY_TEAM_NAME = "FANTASY_TEAM_NAME";
    String JSON_QP = "QP";
    String JSON_AB = "AB";
    String JSON_BA = "BA";
    String JSON_R = "R";
    String JSON_H = "H";
    String JSON_HR = "HR";
    String JSON_RBI = "RBI";
    String JSON_SB = "SB";
    String JSON_IP = "IP";
    String JSON_ER = "ER";
    String JSON_W = "W";
    String JSON_SV = "SV";
    String JSON_BB = "BB";
    String JSON_K = "K";
    String JSON_ERA = "ERA";
    String JSON_WHIP = "WHIP";
    String JSON_CONTRACT = "CONTRACT";
    String JSON_SALARY = "SALARY";
    String JSON_NOTES = "NOTES";
    String JSON_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    String JSON_NATION_OF_BIRTH = "NATION_OF_BIRTH";
    String JSON_ESTIMATED_VALUE = "ESTIMATED_VALUE";
    String JSON_EXT = ".json";
    String SLASH = "/";
    String JSON_TEAM_NAME = "TEAM_NAME";
    String JSON_TEAM_OWNER = "TEAM_OWNER";
    String JSON_STARTING_LINEUP = "STARTING_LINEUP";
    String JSON_VISIBLE_STARTING_LINEUP = "VISIBLE_STARTING_LINEUP";
    String JSON_TAXI_SQUAD = "TAXI_SQUAD";
    
    
    /**
     * Loads the draftToLoad argument using the data found in the json file.
     * 
     * @param draftToLoad Draft to load.
     * @param jsonFilePath File containing the data to load.
     * 
     * @throws IOException Thrown when IO fails.
     */
    @Override
    public void loadDraft(Draft draftToLoad, String jsonFilePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(jsonFilePath);
        
        //LOAD THE DRAFT
        draftToLoad.emptyTeams();
        draftToLoad.emptyDraftedPlayers();
        //LOAD IN THE FANTASY TEAMS ARRAY FOR THE DRAFT
        JsonArray jsonFantasyTeamsArray = json.getJsonArray(JSON_TEAMS);
        for(int i = 0; i < jsonFantasyTeamsArray.size(); i++){
            JsonObject jsfto = jsonFantasyTeamsArray.getJsonObject(i);
            FantasyTeam ft = new FantasyTeam();
            ft.setName(jsfto.getString(JSON_TEAM_NAME));
            ft.setOwnerName(jsfto.getString(JSON_TEAM_OWNER));
            JsonArray jsonVisibleStartingLineUpArray = jsfto.getJsonArray(JSON_VISIBLE_STARTING_LINEUP);
            for(int j = 0; j < jsonVisibleStartingLineUpArray.size(); j++){
                JsonObject jspo = jsonVisibleStartingLineUpArray.getJsonObject(j);
                if(jspo.getString(JSON_PLAYER_TYPE).equals("Hitter")){
                    Hitter h = new Hitter();
                    h.setPlayerType(jspo.getString(JSON_PLAYER_TYPE));
                    h.setPosition(jspo.getString(JSON_POSITION));
                    h.setFirstName(jspo.getString(JSON_FIRST_NAME));
                    h.setLastName(jspo.getString(JSON_LAST_NAME));
                    h.setFantasyTeamName(jspo.getString(JSON_FANTASY_TEAM_NAME));
                    h.setTeamMLB(jspo.getString(JSON_TEAM_MLB));
                    h.setContract(jspo.getString(JSON_CONTRACT));
                    h.setSalary(Integer.parseInt(jspo.getString(JSON_SALARY)));
                    h.setNotes(jspo.getString(JSON_NOTES));
                    h.setYearOfBirth(jspo.getString(JSON_YEAR_OF_BIRTH));
                    h.setNationOfBirth(jspo.getString(JSON_NATION_OF_BIRTH));
                    h.setEstimatedValue(Integer.parseInt(jspo.getString(JSON_ESTIMATED_VALUE)));
                    h.setQP(jspo.getString(JSON_QP));
                    h.setR(Integer.parseInt(jspo.getString(JSON_R)));
                    h.setH(Double.parseDouble(jspo.getString(JSON_R)));
                    h.setHR(Integer.parseInt(jspo.getString(JSON_HR)));
                    h.setRBI(Integer.parseInt(jspo.getString(JSON_RBI)));
                    h.setSB(Integer.parseInt(jspo.getString(JSON_SB)));
                    h.setAB(Integer.parseInt(jspo.getString(JSON_AB)));
                    h.setBA(Double.parseDouble(jspo.getString(JSON_BA)));
                    ft.addToVisibleStartingLineUp(h);
                    draftToLoad.addToDraftedPlayers(h);
                }
                else{
                    Pitcher p = new Pitcher();
                    p.setPosition(jspo.getString(JSON_POSITION));
                    p.setFirstName(jspo.getString(JSON_FIRST_NAME));
                    p.setLastName(jspo.getString(JSON_LAST_NAME));
                    p.setFantasyTeamName(jspo.getString(JSON_FANTASY_TEAM_NAME));
                    p.setTeamMLB(jspo.getString(JSON_TEAM_MLB));
                    p.setContract(jspo.getString(JSON_CONTRACT));
                    p.setSalary(Integer.parseInt(jspo.getString(JSON_SALARY)));
                    p.setNotes(jspo.getString(JSON_NOTES));
                    p.setYearOfBirth(jspo.getString(JSON_YEAR_OF_BIRTH));
                    p.setNationOfBirth(jspo.getString(JSON_NATION_OF_BIRTH));
                    p.setEstimatedValue(Integer.parseInt(jspo.getString(JSON_ESTIMATED_VALUE)));
                    p.setW(Integer.parseInt(jspo.getString(JSON_W)));
                    p.setSV(Integer.parseInt(jspo.getString(JSON_SV)));
                    p.setK(Integer.parseInt(jspo.getString(JSON_K)));
                    p.setER(Integer.parseInt(jspo.getString(JSON_ER)));
                    p.setH(Integer.parseInt(jspo.getString(JSON_H)));
                    p.setBB(Integer.parseInt(jspo.getString(JSON_BB)));
                    p.setIP(Double.parseDouble(jspo.getString(JSON_IP)));
                    p.setERA(Double.parseDouble(jspo.getString(JSON_ERA)));
                    p.setWHIP(Double.parseDouble(jspo.getString(JSON_WHIP)));
                    ft.addToVisibleStartingLineUp(p);
                    draftToLoad.addToDraftedPlayers(p);
                }
            }
            JsonArray jsonTaxiSquadArray = jsfto.getJsonArray(JSON_TAXI_SQUAD);
            for(int k = 0; k < jsonTaxiSquadArray.size(); k++){
                JsonObject jstso = jsonTaxiSquadArray.getJsonObject(k);
                if(jstso.getString(JSON_PLAYER_TYPE).equals("Hitter")){
                    Hitter h = new Hitter();
                    h.setPlayerType(jstso.getString(JSON_PLAYER_TYPE));
                    h.setPosition(jstso.getString(JSON_POSITION));
                    h.setFirstName(jstso.getString(JSON_FIRST_NAME));
                    h.setLastName(jstso.getString(JSON_LAST_NAME));
                    h.setFantasyTeamName(jstso.getString(JSON_FANTASY_TEAM_NAME));
                    h.setTeamMLB(jstso.getString(JSON_TEAM_MLB));
                    h.setContract(jstso.getString(JSON_CONTRACT));
                    h.setSalary(Integer.parseInt(jstso.getString(JSON_SALARY)));
                    h.setNotes(jstso.getString(JSON_NOTES));
                    h.setYearOfBirth(jstso.getString(JSON_YEAR_OF_BIRTH));
                    h.setNationOfBirth(jstso.getString(JSON_NATION_OF_BIRTH));
                    h.setEstimatedValue(Integer.parseInt(jstso.getString(JSON_ESTIMATED_VALUE)));
                    h.setQP(jstso.getString(JSON_QP));
                    h.setR(Integer.parseInt(jstso.getString(JSON_R)));
                    h.setH(Double.parseDouble(jstso.getString(JSON_R)));
                    h.setHR(Integer.parseInt(jstso.getString(JSON_HR)));
                    h.setRBI(Integer.parseInt(jstso.getString(JSON_RBI)));
                    h.setSB(Integer.parseInt(jstso.getString(JSON_SB)));
                    h.setAB(Integer.parseInt(jstso.getString(JSON_AB)));
                    h.setBA(Double.parseDouble(jstso.getString(JSON_BA)));
                    ft.addToVisibleStartingLineUp(h);
                    draftToLoad.addToDraftedPlayers(h);
                }
                else{
                    Pitcher p = new Pitcher();
                    p.setPosition(jstso.getString(JSON_POSITION));
                    p.setFirstName(jstso.getString(JSON_FIRST_NAME));
                    p.setLastName(jstso.getString(JSON_LAST_NAME));
                    p.setFantasyTeamName(jstso.getString(JSON_FANTASY_TEAM_NAME));
                    p.setTeamMLB(jstso.getString(JSON_TEAM_MLB));
                    p.setContract(jstso.getString(JSON_CONTRACT));
                    p.setSalary(Integer.parseInt(jstso.getString(JSON_SALARY)));
                    p.setNotes(jstso.getString(JSON_NOTES));
                    p.setYearOfBirth(jstso.getString(JSON_YEAR_OF_BIRTH));
                    p.setNationOfBirth(jstso.getString(JSON_NATION_OF_BIRTH));
                    p.setEstimatedValue(Integer.parseInt(jstso.getString(JSON_ESTIMATED_VALUE)));
                    p.setW(Integer.parseInt(jstso.getString(JSON_W)));
                    p.setSV(Integer.parseInt(jstso.getString(JSON_SV)));
                    p.setK(Integer.parseInt(jstso.getString(JSON_K)));
                    p.setER(Integer.parseInt(jstso.getString(JSON_ER)));
                    p.setH(Integer.parseInt(jstso.getString(JSON_H)));
                    p.setBB(Integer.parseInt(jstso.getString(JSON_BB)));
                    p.setIP(Double.parseDouble(jstso.getString(JSON_IP)));
                    p.setERA(Double.parseDouble(jstso.getString(JSON_ERA)));
                    p.setWHIP(Double.parseDouble(jstso.getString(JSON_WHIP)));
                    ft.addToVisibleStartingLineUp(p);
                    draftToLoad.addToDraftedPlayers(p);
                }
            }
            
            draftToLoad.addTeam(ft);
        }
        draftToLoad.calculateAndAssignTotalPoints();
        
        //LOAD IN THE PLAYERS ARRAY FOR THE DRAFT
        draftToLoad.emptyPlayers();
        JsonArray jsonPlayersArray = json.getJsonArray(JSON_PLAYERS);
        for(int i = 0; i < jsonPlayersArray.size(); i++){
            JsonObject jspo = jsonPlayersArray.getJsonObject(i);
                if(jspo.getString(JSON_PLAYER_TYPE).equals("Hitter")){
                    Hitter h = new Hitter();
                    h.setPosition(jspo.getString(JSON_POSITION));
                    h.setFirstName(jspo.getString(JSON_FIRST_NAME));
                    h.setLastName(jspo.getString(JSON_LAST_NAME));
                    h.setFantasyTeamName(jspo.getString(JSON_FANTASY_TEAM_NAME));
                    h.setTeamMLB(jspo.getString(JSON_TEAM_MLB));
                    h.setContract(jspo.getString(JSON_CONTRACT));
                    h.setSalary(Integer.parseInt(jspo.getString(JSON_SALARY)));
                    h.setNotes(jspo.getString(JSON_NOTES));
                    h.setYearOfBirth(jspo.getString(JSON_YEAR_OF_BIRTH));
                    h.setNationOfBirth(jspo.getString(JSON_NATION_OF_BIRTH));
                    h.setEstimatedValue(Integer.parseInt(jspo.getString(JSON_ESTIMATED_VALUE)));
                    h.setQP(jspo.getString(JSON_QP));
                    h.setR(Integer.parseInt(jspo.getString(JSON_R)));
                    h.setH(Double.parseDouble(jspo.getString(JSON_H)));
                    h.setHR(Integer.parseInt(jspo.getString(JSON_HR)));
                    h.setRBI(Integer.parseInt(jspo.getString(JSON_RBI)));
                    h.setSB(Integer.parseInt(jspo.getString(JSON_SB)));
                    h.setAB(Integer.parseInt(jspo.getString(JSON_AB)));
                    h.setBA(Double.parseDouble(jspo.getString(JSON_BA)));
                    draftToLoad.addPlayer(h);
                }
                else{
                    Pitcher p = new Pitcher();
                    p.setPosition(jspo.getString(JSON_POSITION));
                    p.setFirstName(jspo.getString(JSON_FIRST_NAME));
                    p.setLastName(jspo.getString(JSON_LAST_NAME));
                    p.setFantasyTeamName(jspo.getString(JSON_FANTASY_TEAM_NAME));
                    p.setTeamMLB(jspo.getString(JSON_TEAM_MLB));
                    p.setContract(jspo.getString(JSON_CONTRACT));
                    p.setSalary(Integer.parseInt(jspo.getString(JSON_SALARY)));
                    p.setNotes(jspo.getString(JSON_NOTES));
                    p.setYearOfBirth(jspo.getString(JSON_YEAR_OF_BIRTH));
                    p.setNationOfBirth(jspo.getString(JSON_NATION_OF_BIRTH));
                    p.setEstimatedValue(Integer.parseInt(jspo.getString(JSON_ESTIMATED_VALUE)));
                    p.setW(Integer.parseInt(jspo.getString(JSON_W)));
                    p.setSV(Integer.parseInt(jspo.getString(JSON_SV)));
                    p.setK(Integer.parseInt(jspo.getString(JSON_K)));
                    p.setER(Integer.parseInt(jspo.getString(JSON_ER)));
                    p.setH(Integer.parseInt(jspo.getString(JSON_H)));
                    p.setBB(Integer.parseInt(jspo.getString(JSON_BB)));
                    p.setIP(Double.parseDouble(jspo.getString(JSON_IP)));
                    p.setERA(Double.parseDouble(jspo.getString(JSON_ERA)));
                    p.setWHIP(Double.parseDouble(jspo.getString(JSON_WHIP)));
                    draftToLoad.addPlayer(p);
                }

            
        }
    }
        

    /**
     * This method saves all the data associated with a course to
     * a JSON file.
     * 
     * @param courseToSave The course whose data we are saving.
     * 
     * @throws IOException Thrown when there are issues writing
     * to the JSON file.
     */
    @Override
    public void saveDraft(Draft draftToSave, String draftName) throws IOException {
        // BUILD THE FILE PATH
        String jsonFilePath = PATH_DRAFTS + SLASH + draftName + JSON_EXT;
        
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);
        
        //MAKE A JSON ARRAY FOR THE FANTASY TEAMS ARRAY
        JsonArray JsonTeamsArray = makeFantasyTeamsJsonArray(draftToSave.getTeams());
        
        //MAKE A JSON ARRAY FOR THE PLAYERS ARRAY
        JsonArray JsonPlayersArray = makePlayersJsonArray(draftToSave.getPlayersList());
        
        //BUILD DRAFT WITH EVERYTHING
        JsonObject draftJsonObject = Json.createObjectBuilder()
                                    .add(JSON_TEAMS, JsonTeamsArray)
                                    .add(JSON_PLAYERS, JsonPlayersArray)
                                    .build();
        
        //SAVE EVERYTHING
        jsonWriter.writeObject(draftJsonObject);
    }

    
    @Override
    public void loadHitters(String hittersFilePath, Draft draftToLoadInto) throws IOException {
        JsonObject json = loadJSONFile(hittersFilePath);
        //GET THE HITTERS
        JsonArray jsonHittersArray = json.getJsonArray(JSON_HITTERS);
        for(int i = 0; i < jsonHittersArray.size(); i++){
            JsonObject jso = jsonHittersArray.getJsonObject(i);
            Hitter h = new Hitter();
            h.setTeamMLB(jso.getString(JSON_TEAM));
            h.setLastName(jso.getString(JSON_LAST_NAME));
            h.setFirstName(jso.getString(JSON_FIRST_NAME));
            h.setQP(jso.getString(JSON_QP));
            //add U to QP
            h.setQP(h.getQP() + "_U");
            //add MI or CI to QP if appropriate
            if(h.getQP().contains("1B") || h.getQP().contains("3B")){
                h.setQP(h.getQP() + "_CI");
            }
            if(h.getQP().contains("2B") || h.getQP().contains("SS")){
                h.setQP(h.getQP() + "_MI");
            }
            h.setAB(Integer.parseInt(jso.getString(JSON_AB)));
            h.setR(Integer.parseInt(jso.getString(JSON_R)));
            h.setH(Double.parseDouble(jso.getString(JSON_H)));
            h.setHR(Integer.parseInt(jso.getString(JSON_HR)));
            h.setRBI(Integer.parseInt(jso.getString(JSON_RBI)));
            h.setSB(Integer.parseInt(jso.getString(JSON_SB)));
            h.setNotes(jso.getString(JSON_NOTES));
            h.setYearOfBirth(jso.getString(JSON_YEAR_OF_BIRTH));
            h.setNationOfBirth(jso.getString(JSON_NATION_OF_BIRTH));
            //CALCULATE AND THE BA VALUE FOR THE HITTER
            h.calculateAndSetBA();
            //ADD THE HITTER TO THE UNSELECTED PLAYERS ARRAY
            draftToLoadInto.addPlayer(h);
        }
    }
    
    @Override
    public void loadPitchers(String pitchersFilePath, Draft draftToLoadInto) throws IOException{
        JsonObject json = loadJSONFile(pitchersFilePath);
        //GET THE HITTERS
        JsonArray jsonPitchersArray = json.getJsonArray(JSON_PITCHERS);
        for(int i = 0; i < jsonPitchersArray.size(); i++){
            JsonObject jso = jsonPitchersArray.getJsonObject(i);
            Pitcher p = new Pitcher();
            p.setTeamMLB(jso.getString(JSON_TEAM));
            p.setLastName(jso.getString(JSON_LAST_NAME));
            p.setFirstName(jso.getString(JSON_FIRST_NAME));
            p.setIP(Double.parseDouble(jso.getString(JSON_IP)));
            p.setER(Integer.parseInt(jso.getString(JSON_ER)));
            p.setW(Integer.parseInt(jso.getString(JSON_W)));
            p.setSV(Integer.parseInt(jso.getString(JSON_SV)));
            p.setH(Integer.parseInt(jso.getString(JSON_H)));
            p.setBB(Integer.parseInt(jso.getString(JSON_BB)));
            p.setK(Integer.parseInt(jso.getString(JSON_K)));
            p.setNotes(jso.getString(JSON_NOTES));
            p.setYearOfBirth(jso.getString(JSON_YEAR_OF_BIRTH));
            p.setNationOfBirth(jso.getString(JSON_NATION_OF_BIRTH));
            
            //CALCULATE AND SET THE WHIP AND ERA VALUES
            p.calculateAndSetERA();
            p.calculateAndSetWHIP();
            
            //ADD THE PITCHER TO THE UNSELECTED PLAYERS ARRAY
            draftToLoadInto.addPlayer(p);
        }
    }
    
    
    
    // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
    
    //MAKE A JSON ARRAY OBJECT FOR FANTASY TEAMS
    private JsonArray makeFantasyTeamsJsonArray(ObservableList<FantasyTeam> ftl){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(FantasyTeam ft : ftl){
            jsb.add(makeFantasyTeamJsonObject(ft));
        }
        JsonArray ja = jsb.build();
        return ja;
    }
    
    //MAKE AND RETURN A FANTASY TEAM JSON OBJECT
    private JsonObject makeFantasyTeamJsonObject(FantasyTeam ft){
        //MAKE JSON ARRAY FOR VISIBLE STARTING LINEUP
        JsonArray jsonVisibleStartingLineUp = makePlayersJsonArray(ft.getVisibleStartingLineUp());
        JsonArray jsonTaxiSquad = makePlayersJsonArray(ft.getTaxiSquad());
        
        //BUILD FANTASY TEAM WITH EVERYTHING MADE
        JsonObject jso = Json.createObjectBuilder()
                         .add(JSON_TEAM_NAME, ft.getName())
                         .add(JSON_TEAM_OWNER, ft.getOwnerName())
                         .add(JSON_VISIBLE_STARTING_LINEUP, jsonVisibleStartingLineUp)
                         .add(JSON_TAXI_SQUAD, jsonTaxiSquad)
                         .build();
        return jso;
    }
    
    // MAKE AN ARRAY OF PLAYERS
    private JsonArray makePlayersJsonArray(ObservableList<Player> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Player p : data) {
            if(p instanceof Hitter){
                jsb.add(makeHitterJsonObject((Hitter)p));
            }
            else{
                jsb.add(makePitcherJsonObject((Pitcher)p));
            }
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    
    //MAKES AND RETURNS A JSON OBJECT FOR A PLAYER
    private JsonObject makeHitterJsonObject(Hitter hitter){
        JsonObject jso = Json.createObjectBuilder()
                                                   .add(JSON_PLAYER_TYPE, hitter.getPlayerType())
                                                   .add(JSON_POSITION, hitter.getPosition())
                                                   .add(JSON_FIRST_NAME, hitter.getFirstName())
                                                   .add(JSON_LAST_NAME, hitter.getLastName())
                                                   .add(JSON_FANTASY_TEAM_NAME, hitter.getFantasyTeamName())
                                                   .add(JSON_TEAM_MLB, hitter.getTeamMLB())
                                                   .add(JSON_CONTRACT, hitter.getContract())
                                                   .add(JSON_SALARY, Integer.toString(hitter.getSalary()))
                                                   .add(JSON_NOTES, hitter.getNotes())
                                                   .add(JSON_YEAR_OF_BIRTH, hitter.getYearOfBirth())
                                                   .add(JSON_NATION_OF_BIRTH, hitter.getNationOfBirth())
                                                   .add(JSON_ESTIMATED_VALUE, Integer.toString(hitter.getEstimatedValue()))
                                                   .add(JSON_QP, hitter.getQP())
                                                   .add(JSON_R, Integer.toString(hitter.getR()))
                                                   .add(JSON_H, Double.toString(hitter.getH()))
                                                   .add(JSON_HR, Integer.toString(hitter.getHR()))
                                                   .add(JSON_RBI, Integer.toString(hitter.getRBI()))
                                                   .add(JSON_SB, Integer.toString(hitter.getSB()))
                                                   .add(JSON_AB, Integer.toString(hitter.getAB()))
                                                   .add(JSON_BA, Double.toString(hitter.getBA()))
                                                   .build();
        return jso;
    }
    
    //MAKES AND RETURNS A JSON OBJECT FOR A PLAYER
    private JsonObject makePitcherJsonObject(Pitcher pitcher){
        JsonObject jso = Json.createObjectBuilder()
                                                   .add(JSON_PLAYER_TYPE, pitcher.getPlayerType())
                                                   .add(JSON_POSITION, pitcher.getPosition())
                                                   .add(JSON_FIRST_NAME, pitcher.getFirstName())
                                                   .add(JSON_LAST_NAME, pitcher.getLastName())
                                                   .add(JSON_FANTASY_TEAM_NAME, pitcher.getFantasyTeamName())
                                                   .add(JSON_TEAM_MLB, pitcher.getTeamMLB())
                                                   .add(JSON_CONTRACT, pitcher.getContract())
                                                   .add(JSON_SALARY, Integer.toString(pitcher.getSalary()))
                                                   .add(JSON_NOTES, pitcher.getNotes())
                                                   .add(JSON_YEAR_OF_BIRTH, pitcher.getYearOfBirth())
                                                   .add(JSON_NATION_OF_BIRTH, pitcher.getNationOfBirth())
                                                   .add(JSON_ESTIMATED_VALUE, Integer.toString(pitcher.getEstimatedValue()))
                                                   .add(JSON_W, Integer.toString(pitcher.getW()))
                                                   .add(JSON_SV, Integer.toString(pitcher.getSV()))
                                                   .add(JSON_K, Integer.toString(pitcher.getK()))
                                                   .add(JSON_ER, Integer.toString(pitcher.getER()))
                                                   .add(JSON_H, Integer.toString(pitcher.getH()))
                                                   .add(JSON_BB, Integer.toString(pitcher.getBB()))
                                                   .add(JSON_IP, Double.toString(pitcher.getIP()))
                                                   .add(JSON_ERA, Double.toString(pitcher.getERA()))
                                                   .add(JSON_WHIP, Double.toString(pitcher.getWHIP()))
                                                   .build();
        return jso;
    }
    
        
    
    
}



