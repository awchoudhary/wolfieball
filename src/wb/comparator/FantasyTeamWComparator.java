package wb.comparator;

import java.util.Comparator;
import wb.data.FantasyTeam;

/**
 *
 * @author Awaes
 */
public class FantasyTeamWComparator implements Comparator<FantasyTeam>{
    public int compare(FantasyTeam first, FantasyTeam second){
        if(first.getW() < second.getW()){
            return -1;
        }
        else if(first.getW() == second.getW()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
