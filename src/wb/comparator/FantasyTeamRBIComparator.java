package wb.comparator;

import java.util.Comparator;
import wb.data.FantasyTeam;

/**
 *
 * @author Awaes
 */
public class FantasyTeamRBIComparator implements Comparator<FantasyTeam>{
    public int compare(FantasyTeam first, FantasyTeam second){
        if(first.getRBI() < second.getRBI()){
            return -1;
        }
        else if(first.getRBI() == second.getRBI()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
