package wb.comparator;

import java.util.Comparator;
import wb.data.Hitter;

/**
 *
 * @author Awaes
 */
public class HitterRComparator implements Comparator<Hitter>{
    public int compare(Hitter first, Hitter second){
        if(first.getR() < second.getR()){
            return -1;
        }
        else if(first.getR() == second.getR()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
