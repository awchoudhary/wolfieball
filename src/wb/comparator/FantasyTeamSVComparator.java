package wb.comparator;

import java.util.Comparator;
import wb.data.FantasyTeam;

/**
 *
 * @author Awaes
 */
public class FantasyTeamSVComparator implements Comparator<FantasyTeam>{
    public int compare(FantasyTeam first, FantasyTeam second){
        if(first.getSV() < second.getSV()){
            return -1;
        }
        else if(first.getSV() == second.getSV()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
