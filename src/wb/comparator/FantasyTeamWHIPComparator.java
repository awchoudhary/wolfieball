package wb.comparator;

import java.util.Comparator;
import wb.data.FantasyTeam;

/**
 *
 * @author Awaes
 */
public class FantasyTeamWHIPComparator implements Comparator<FantasyTeam>{
    public int compare(FantasyTeam first, FantasyTeam second){
        if(first.getWHIP() > second.getWHIP()){
            return -1;
        }
        else if(first.getWHIP() == second.getWHIP()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
