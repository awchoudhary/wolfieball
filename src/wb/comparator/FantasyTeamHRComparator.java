package wb.comparator;

import java.util.Comparator;
import wb.data.FantasyTeam;

/**
 *
 * @author Awaes
 */
public class FantasyTeamHRComparator implements Comparator<FantasyTeam>{
    public int compare(FantasyTeam first, FantasyTeam second){
        if(first.getHR() < second.getHR()){
            return -1;
        }
        else if(first.getHR() == second.getHR()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
