package wb.comparator;

import java.util.Comparator;
import wb.data.FantasyTeam;

/**
 *
 * @author Awaes
 */
public class FantasyTeamBAComparator implements Comparator<FantasyTeam>{
    public int compare(FantasyTeam first, FantasyTeam second){
        if(first.getBA() < second.getBA()){
            return -1;
        }
        else if(first.getBA() == second.getBA()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
