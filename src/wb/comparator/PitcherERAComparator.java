package wb.comparator;

import java.util.Comparator;
import wb.data.Pitcher;

/**
 *
 * @author Awaes
 */
public class PitcherERAComparator implements Comparator<Pitcher>{
    public int compare(Pitcher first, Pitcher second){
        if(first.getERA() > second.getERA()){
            return -1;
        }
        else if(first.getERA() == second.getERA()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
