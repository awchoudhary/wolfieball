package wb.comparator;

import java.util.Comparator;
import wb.data.Hitter;

/**
 *
 * @author Awaes
 */
public class HitterHRComparator implements Comparator<Hitter>{
    public int compare(Hitter first, Hitter second){
        if(first.getHR() < second.getHR()){
            return -1;
        }
        else if(first.getHR() == second.getHR()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
