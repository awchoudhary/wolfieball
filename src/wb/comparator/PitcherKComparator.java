package wb.comparator;

import java.util.Comparator;
import wb.data.Pitcher;

/**
 *
 * @author Awaes
 */
public class PitcherKComparator implements Comparator<Pitcher>{
    public int compare(Pitcher first, Pitcher second){
        if(first.getK() < second.getK()){
            return -1;
        }
        else if(first.getK() == second.getK()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
