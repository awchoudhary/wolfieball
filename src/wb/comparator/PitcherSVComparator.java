package wb.comparator;

import java.util.Comparator;
import wb.data.Pitcher;

/**
 *
 * @author Awaes
 */
public class PitcherSVComparator implements Comparator<Pitcher>{
    public int compare(Pitcher first, Pitcher second){
        if(first.getSV() < second.getSV()){
            return -1;
        }
        else if(first.getSV() == second.getSV()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
