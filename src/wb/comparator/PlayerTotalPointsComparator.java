package wb.comparator;

import java.util.Comparator;
import wb.data.Player;

/**
 *
 * @author Awaes
 */
public class PlayerTotalPointsComparator implements Comparator<Player>{
    public int compare(Player first, Player second){
        if(first.getTotalPoints() > second.getTotalPoints()){
            return -1;
        }
        else if(first.getTotalPoints() == second.getTotalPoints()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
