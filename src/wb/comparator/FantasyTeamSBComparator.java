package wb.comparator;

import java.util.Comparator;
import wb.data.FantasyTeam;

/**
 *
 * @author Awaes
 */
public class FantasyTeamSBComparator implements Comparator<FantasyTeam>{
    public int compare(FantasyTeam first, FantasyTeam second){
        if(first.getSB() < second.getSB()){
            return -1;
        }
        else if(first.getSB() == second.getSB()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
