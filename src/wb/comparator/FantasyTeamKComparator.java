package wb.comparator;

import java.util.Comparator;
import wb.data.FantasyTeam;

/**
 *
 * @author Awaes
 */
public class FantasyTeamKComparator implements Comparator<FantasyTeam>{
    public int compare(FantasyTeam first, FantasyTeam second){
        if(first.getK() < second.getK()){
            return -1;
        }
        else if(first.getK() == second.getK()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
