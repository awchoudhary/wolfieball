package wb.comparator;

import java.util.Comparator;
import wb.data.FantasyTeam;

/**
 *
 * @author Awaes
 */
public class FantasyTeamERAComparator implements Comparator<FantasyTeam>{
    public int compare(FantasyTeam first, FantasyTeam second){
        if(first.getERA() > second.getERA()){
            return -1;
        }
        else if(first.getERA() == second.getERA()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
