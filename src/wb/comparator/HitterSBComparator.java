package wb.comparator;

import java.util.Comparator;
import wb.data.Hitter;

/**
 *
 * @author Awaes
 */
public class HitterSBComparator implements Comparator<Hitter>{
    public int compare(Hitter first, Hitter second){
        if(first.getSB() < second.getSB()){
            return -1;
        }
        else if(first.getSB() == second.getSB()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
