package wb.comparator;

import java.util.Comparator;
import wb.data.FantasyTeam;

/**
 *
 * @author Awaes
 */
public class FantasyTeamRComparator implements Comparator<FantasyTeam>{
    public int compare(FantasyTeam first, FantasyTeam second){
        if(first.getR() < second.getR()){
            return -1;
        }
        else if(first.getR() == second.getR()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
