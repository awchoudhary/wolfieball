package wb.comparator;

import java.util.Comparator;
import wb.data.Pitcher;

/**
 *
 * @author Awaes
 */
public class PitcherWComparator implements Comparator<Pitcher>{
    public int compare(Pitcher first, Pitcher second){
        if(first.getW() < second.getW()){
            return -1;
        }
        else if(first.getW() == second.getW()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
