package wb.comparator;

import java.util.Comparator;
import wb.data.Pitcher;

/**
 *
 * @author Awaes
 */
public class PitcherWHIPComparator implements Comparator<Pitcher>{
    public int compare(Pitcher first, Pitcher second){
        if(first.getWHIP() > second.getWHIP()){
            return -1;
        }
        else if(first.getWHIP() == second.getWHIP()){
            return 0;
        }
        else{
            return 1;
        }
    }
}
