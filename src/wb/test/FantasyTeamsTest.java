package wb.test;

import wb.data.*;

/**
 *
 * @author Awaes
 */
public class FantasyTeamsTest {
    public static void main(String[] args){
      FantasyTeam myTeam = new FantasyTeam();
      Hitter h1 = new Hitter();
      h1.setFirstName("Bilkees");
      h1.setQP("C");
      Hitter h2 = new Hitter();
      h2.setFirstName("BilkeesKaur");
      h2.setQP("C");
      Hitter h3 = new Hitter();
      h3.setFirstName("Roman");
      h3.setQP("1B");
      Hitter h4 = new Hitter();
      h4.setFirstName("OConner");
      h4.setQP("OF");
      Pitcher p1 = new Pitcher();
      p1.setFirstName("Dom");
      p1.setRole("P");
      
      myTeam.addToVisibleStartingLineUp(h3);
      myTeam.addToVisibleStartingLineUp(h1);
      myTeam.addToVisibleStartingLineUp(h4);
      myTeam.addToVisibleStartingLineUp(p1);
      myTeam.addToVisibleStartingLineUp(h2);
      
      myTeam.removeFromVisibleStartingLineUp(h3);
      printTeam(myTeam);
      
      
    }
    
    public static void printTeam(FantasyTeam a){
        for(int i = 0; i < a.getVisibleStartingLineUp().size(); i++){
            System.out.println(a.getVisibleStartingLineUp().get(i).getFirstName());
        }
    }
    
    
}
