package wb.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wb.data.Hitter;
import wb.data.Pitcher;
import wb.data.Player;
import static wb.gui.PlayersScreenGui.CLASS_PROMPT_LABEL;
import static wb.gui.SwitchScreenGui.CLASS_HEADING_LABEL;
import static wb.gui.SwitchScreenGui.PRIMARY_STYLE_SHEET;

/**
 *
 * @author Awaes
 */
public class AddPlayerDialog extends Stage{
    Pitcher p = new Pitcher();
    Hitter h = new Hitter();
    
    //GUI CONTRLS
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label firstNameLabel;
    TextField firstNameTextField;
    Label lastNameLabel;
    TextField lastNameTextField;
    Label proTeamLabel;
    ComboBox proTeamComboBox;
    HBox checkBoxPane;
    CheckBox checkBoxC;
    CheckBox checkBox1B;
    CheckBox checkBox3B;
    CheckBox checkBox2B;
    CheckBox checkBoxSS;
    CheckBox checkBoxOF;
    CheckBox checkBoxP;
    Button completeButton;
    Button cancelButton;
    
    //keeps track of which button was pressed
    String selection = new String();
    
    // CONSTANTS FOR OUR UI
    public static final String DIALOG_HEADING = "Add Player";
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FIRST_NAME_PROMPT = "First Name:";
    public static final String LAST_NAME_PROMPT = "Last Name:";
    public static final String PRO_TEAM_PROMPT = "Pro Team:";
    public static final String PROMPT_C = "C";
    public static final String PROMPT_1B = "1B";
    public static final String PROMPT_3B = "3B";
    public static final String PROMPT_2B = "2B";
    public static final String PROMPT_SS = "SS";
    public static final String PROMPT_OF = "OF";
    public static final String PROMPT_P = "P";
    
    public AddPlayerDialog(Stage primaryStage) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(DIALOG_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE NAME TEXTFIELDS
        firstNameLabel = new Label(FIRST_NAME_PROMPT);
        firstNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        firstNameTextField = new TextField();
        firstNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            h.setFirstName(newValue);
            p.setFirstName(newValue);
        });
        
        lastNameLabel = new Label(LAST_NAME_PROMPT);
        lastNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        lastNameTextField = new TextField();
        lastNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            h.setLastName(newValue);
            p.setLastName(newValue);
        });
        
        //INIT PRO TEAM COMBOBOX
        proTeamLabel = new Label(PRO_TEAM_PROMPT);
        proTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        proTeamComboBox = new ComboBox();
        ObservableList<String> positions = FXCollections.observableArrayList();
        positions.add("ATL");
        positions.add("AZ");
        positions.add("CHC");
        positions.add("CIN");
        positions.add("COL");
        positions.add("LAD");
        positions.add("MIA");
        positions.add("MIL");
        positions.add("NYM");
        positions.add("PHI");
        positions.add("PIT");
        positions.add("SD");
        positions.add("SF");
        positions.add("STL");
        positions.add("WAS");
        proTeamComboBox.setItems(positions);
        proTeamComboBox.setOnAction( e -> {
            h.setTeamMLB(proTeamComboBox.getValue().toString());
            p.setTeamMLB(proTeamComboBox.getValue().toString());
        });
        
        //CHECKBOXES FOR POSITIONS
        checkBoxPane = new HBox();
        checkBoxC = new CheckBox(PROMPT_C);
        checkBox1B = new CheckBox(PROMPT_1B);
        checkBox3B = new CheckBox(PROMPT_3B);
        checkBox2B = new CheckBox(PROMPT_2B);
        checkBoxSS = new CheckBox(PROMPT_SS);
        checkBoxOF = new CheckBox(PROMPT_OF);
        checkBoxP = new CheckBox(PROMPT_P);
        checkBoxPane.getChildren().add(checkBoxC);
        checkBoxPane.getChildren().add(checkBox1B);
        checkBoxPane.getChildren().add(checkBox3B);
        checkBoxPane.getChildren().add(checkBox2B);
        checkBoxPane.getChildren().add(checkBoxSS);
        checkBoxPane.getChildren().add(checkBoxOF);
        checkBoxPane.getChildren().add(checkBoxP);
                
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            AddPlayerDialog.this.selection = sourceButton.getText();
            AddPlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(firstNameLabel, 0, 1, 1, 1);
        gridPane.add(firstNameTextField, 1, 1, 1, 1);
        gridPane.add(lastNameLabel, 0, 2, 1, 1);
        gridPane.add(lastNameTextField, 1, 2, 1, 1);
        gridPane.add(proTeamLabel, 0, 3, 1, 1);
        gridPane.add(proTeamComboBox, 1, 3, 1, 1);
        gridPane.add(checkBoxPane, 0, 4);
        gridPane.add(completeButton, 0, 5, 1, 1);
        gridPane.add(cancelButton, 1, 5, 1, 1);
        
        
        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Hitter getHitter() { 
        return h;
    }
    
    public Pitcher getPitcher() { 
        return p;
    }
    
    public void showAddPlayerDialog() {
        // SET THE DIALOG TITLE
        setTitle(DIALOG_HEADING);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        h = new Hitter();
        p = new Pitcher();
        
        // RESET GUI
        firstNameTextField.setText("");
        lastNameTextField.setText("");
        
        // AND OPEN IT UP
        this.showAndWait();
        
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    //METHODS TO CHECK WHICH CHECKBOX IS SELECTED
    public boolean isCheckBoxCSelected(){
        return checkBoxC.isSelected();
    }
    public boolean isCheckBox1BSelected(){
        return checkBox1B.isSelected();
    }
    public boolean isCheckBox3BSelected(){
        return checkBox3B.isSelected();
    }
    public boolean isCheckBox2BSelected(){
        return checkBox2B.isSelected();
    }
    public boolean isCheckBoxSSSelected(){
        return checkBoxSS.isSelected();
    }
    public boolean isCheckBoxOFSelected(){
        return checkBoxOF.isSelected();
    }
    public boolean isCheckBoxPSelected(){
        return checkBoxP.isSelected();
    }

}
