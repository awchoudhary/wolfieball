package wb.gui;

import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wb.WB_StartupConstants.PATH_CSS;
import wb.data.DraftDataManager;
import wb.WB_PropertyType;
import static wb.WB_StartupConstants.CLOSE_BUTTON_LABEL;
import static wb.WB_StartupConstants.PATH_IMAGES;
import wb.controller.FantasyTeamEditController;
import wb.controller.PlayerEditController;
import wb.data.Draft;
import wb.data.Hitter;
import wb.data.Pitcher;
import wb.data.Player;
import static wb.gui.SwitchScreenGui.CLASS_WORK_SPACE;


/**
 * This class provides the Graphical User Interface for the fantasy teams screen,
 * and displays the fantasy teams in the draft, along with their players
 * 
 * @author Awaes Choudhary
 */
public class FantasyTeamsGui {
    
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_PLAYERS_SCREEN = "players_screen";
    static final String CLASS_RADIO_BUTTON_TOOLBAR = "radio_button_toolbar";
    static final String CLASS_FANTASY_TEAMS_BOX = "fantasy_teams_screen_box";
    static final int TEXTFIELD_LENGTH = 30;

    
    //MANAGES ALL OF THE APPLICATIONS DATA
    DraftDataManager dataManager;
    
    //HANDLER FOR FANTASY TEAM EDIT REQUESTS
    FantasyTeamEditController editController;
    PlayerEditController playerEditController;
    
    //PRIMARY STAGE OF APPLICATION
    Stage primaryStage;
    
    //MAIN GUI FOR APPLIACTION
    SwitchScreenGui switchScreenGui;
    PlayersScreenGui playersScreenGui;
    
    //DIALOGS USED IN GUI
    YesNoCancelDialog yesNoCancelDialog;
    MessageDialog messageDialog;
    
    //CONTAINS THE CONTENTS OF THE PAGE
    ScrollPane fantasyTeamsScrollPane;
    VBox fantasyTeamsBox;
    
    //PAGE HEADING
    Label fantasyTeamsLabel;
    
    //FOR EDITING DRAFT NAME
    HBox draftNameBox;
    Label draftNameLabel;
    TextField draftNameTextField;
    
    //GUI COMPONENTS FOR THE TOOLBAR
    HBox toolbar;
    Button plusButton;
    Button minusButton;
    Button editButton;
    Label selectTeamLabel;
    ComboBox selectTeamComboBox;
    
    //GUI COMPONENTS FOR THE PLAYERS TABLE
    VBox startingLineUpBox;
    Label playersTableHeading;
    VBox playersTableBox;
    TableView<Player> playersTable;
    Boolean updatePlayersTable = true;
    TableColumn actualPositionColumn;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn teamColumn;
    TableColumn positionColumn;
    TableColumn RorWColumn;
    TableColumn HRorSVColumn;
    TableColumn RBIorKColumn;
    TableColumn SBorERAColumn;
    TableColumn BAorWHIPColumn;
    TableColumn estimatedValueColumn;
    TableColumn contractColumn;
    TableColumn salaryColumn;
    
    //GUI COMPONENTS FOR TAXI SQUAD TABLE
    VBox taxiSquadBox;
    Label taxiSquadHeading;
    TableView<Player> taxiSquadTable;
    TableColumn taxiFirstNameColumn;
    TableColumn taxiLastNameColumn;
    TableColumn taxiTeamColumn;
    TableColumn taxiPositionColumn;
    TableColumn taxiRorWColumn;
    TableColumn taxiHRorSVColumn;
    TableColumn taxiRBIorKColumn;
    TableColumn taxiSBorERAColumn;
    TableColumn taxiBAorWHIPColumn;
    TableColumn taxiEstimatedValueColumn;
    TableColumn taxiContractColumn;
    TableColumn taxiSalaryColumn;
    
    
    static final String COL_ACTUAL_POSITION = "Position";
    static final String COL_FIRST_NAME = "First Name";
    static final String COL_LAST_NAME = "Last Name";
    static final String COL_TEAM = "Pro Team";
    static final String COL_POSITION = "Positions";
    static final String COL_R_OR_W = "R/W";
    static final String COL_HR_OR_SV = "HR/SV";
    static final String COL_RBI_OR_K = "RBI/K";
    static final String COL_SB_OR_ERA = "SB/ERA";
    static final String COL_BA_OR_WHIP = "BA/WHIP";
    static final String COL_ESTIMATED_VALUE = "Estimated Value";
    static final String COL_CONTRACT = "Contract";
    static final String COL_SALARY = "Salary";
    
    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initSwitchScreenGui.
     *
     * @param initDataManager data manager for gui.**/
    public FantasyTeamsGui(Stage initPrimaryStage, SwitchScreenGui initSwitchScreenGui, DraftDataManager initDataManager){
        dataManager = initDataManager;
        primaryStage = initPrimaryStage;
        switchScreenGui = initSwitchScreenGui;
    }
    
    /**
     * Accessor method for the data manager.
     *
     * @return The DraftDataManager used by this UI.
     */
    public DraftDataManager getDataManager() {
        return dataManager;
    }
    
    /**
     * Accessor method for the fantasy teams pane.
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public ScrollPane getFantasyTeamsBox() {
        return fantasyTeamsScrollPane;
    }
    
    //ACCESSOR FOR TEAM SELECTION COMBOBOX USED IN GUI
    public ComboBox getSelectTeamComboBox(){
        return selectTeamComboBox;
    }
    
    public String getDraftName(){
        return draftNameTextField.getText();
    }
    
    public TextField getDraftNameTextField(){
        return draftNameTextField;
    }
    
    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The CourseDataManager to be used by this UI.
     */
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }
    
    /**
     * This method fully initializes the user interface for use.
     *
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initFantasyTeamsGui(){
        //INIT THE FANTASY TEAMS BOX
        initFantasyTeamsBox();
        
    }
    
     /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/

    //INIT DIALOGS USED IN GUI
    private void initDialogs(){
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
        messageDialog = new MessageDialog(switchScreenGui.getPrimaryStage(), CLOSE_BUTTON_LABEL);
    }
    //INITIALIZES THE FANTASY TEAMS BOX
    private void initFantasyTeamsBox(){
        fantasyTeamsBox = new VBox();
        fantasyTeamsBox.getStyleClass().add(CLASS_FANTASY_TEAMS_BOX);
        initChildLabel(fantasyTeamsBox, WB_PropertyType.FANTASY_TEAMS_LABEL, CLASS_HEADING_LABEL);
        //INIT ALL GUI COMPONENTS FOR THE SCREEN
        initDraftNameBox();
        initStartingLineUpBox();
        initTaxiSquadBox();
        //INIT EVENT HANDLERS AND DIALOGS
        initDialogs();
        initEventHandlers();
        //ADD THEM ALL TO THE BOX
        fantasyTeamsBox.getChildren().add(draftNameBox);
        fantasyTeamsBox.getChildren().add(startingLineUpBox);
        fantasyTeamsBox.getChildren().add(taxiSquadBox);
        //ADD FANTASY TEAMS BOX TO A SCROLLPANE
        fantasyTeamsScrollPane = new ScrollPane();
        fantasyTeamsScrollPane.setContent(fantasyTeamsBox);
        fantasyTeamsScrollPane.setFitToWidth(true);
    }
    
    // INIT A LABEL AND PUT IT IN A TOOLBAR
    private Label initChildLabel(Pane container, WB_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(WB_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    private void initToolbar(){
        toolbar = new HBox();
        plusButton = initChildButton(toolbar, WB_PropertyType.ADD_ICON, WB_PropertyType.ADD_TEAM_TOOLTIP, false);
        minusButton = initChildButton(toolbar, WB_PropertyType.MINUS_ICON, WB_PropertyType.REMOVE_TEAM_TOOLTIP, false);
        editButton = initChildButton(toolbar, WB_PropertyType.EDIT_ICON, WB_PropertyType.EDIT_TEAM_TOOLTIP, false);
        selectTeamLabel = initChildLabel(toolbar, WB_PropertyType.SELECT_TEAM_LABEL, CLASS_PROMPT_LABEL);
        //ADD COMBOBOX TO TOOLBAR
        selectTeamComboBox = new ComboBox();
        selectTeamComboBox.setItems(dataManager.getDraft().getTeamNamesList());
        toolbar.getChildren().add(selectTeamComboBox);
        toolbar.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
    }
    
    //INIT THE DRAFT NAME TOOLBOX
    private void initDraftNameBox(){
        draftNameBox = new HBox();
        draftNameLabel = initChildLabel(draftNameBox, WB_PropertyType.DRAFT_NAME_LABEL, CLASS_PROMPT_LABEL);
        draftNameTextField = initTextField(draftNameBox, TEXTFIELD_LENGTH, true);
        //draftNameBox.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
    }
    
    private void initStartingLineUpBox(){
        //INIT THE PLAYERS TABLE
        playersTable = new TableView();
        //INIT THE PLAYERS TABLE COMLUMN
        actualPositionColumn = new TableColumn(COL_ACTUAL_POSITION);
        firstNameColumn = new TableColumn(COL_FIRST_NAME);
        lastNameColumn = new TableColumn(COL_LAST_NAME);
        teamColumn = new TableColumn(COL_TEAM);
        positionColumn = new TableColumn(COL_POSITION);
        RorWColumn = new TableColumn(COL_R_OR_W);
        HRorSVColumn = new TableColumn(COL_HR_OR_SV);
        RBIorKColumn = new TableColumn(COL_RBI_OR_K);
        SBorERAColumn = new TableColumn(COL_SB_OR_ERA);
        BAorWHIPColumn = new TableColumn(COL_BA_OR_WHIP);
        estimatedValueColumn = new TableColumn(COL_ESTIMATED_VALUE);
        contractColumn = new TableColumn(COL_CONTRACT);
        salaryColumn = new TableColumn(COL_SALARY);
                
        //AND LINK THE COLUMNS TO THE DATA
        actualPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        teamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamMLB"));
        positionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        RorWColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("R"));
        HRorSVColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("HR"));
        RBIorKColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("RBI"));
        SBorERAColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("SB"));
        BAorWHIPColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("BA"));
        estimatedValueColumn.setCellValueFactory(new PropertyValueFactory<String, String>("estimatedValue"));
        contractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        salaryColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("salary"));
                
        //ADD COLUMNS TO TABLE
        playersTable.getColumns().add(actualPositionColumn);
        playersTable.getColumns().add(firstNameColumn);
        playersTable.getColumns().add(lastNameColumn);
        playersTable.getColumns().add(teamColumn);
        playersTable.getColumns().add(positionColumn);
        playersTable.getColumns().add(RorWColumn);
        playersTable.getColumns().add(HRorSVColumn);
        playersTable.getColumns().add(RBIorKColumn);
        playersTable.getColumns().add(SBorERAColumn);
        playersTable.getColumns().add(BAorWHIPColumn);
        playersTable.getColumns().add(estimatedValueColumn);
        playersTable.getColumns().add(contractColumn);
        playersTable.getColumns().add(salaryColumn);
        playersTable.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
        
        //INIT THE STARTING LINE UP BOX
        startingLineUpBox = new VBox();
        //INIT AND ADD TOOLBAR TO BOX
        initToolbar();
        startingLineUpBox.getChildren().add(toolbar);
        //ADD HEADING TO BOX
        playersTableHeading = initChildLabel(startingLineUpBox, WB_PropertyType.STARTING_LINEUP_LABEL, CLASS_SUBHEADING_LABEL);
        //ADD TABLE TO BOX
        startingLineUpBox.getChildren().add(playersTable);
    }
    
    //INIT THE TAXI SQUAD BOX
    private void initTaxiSquadBox(){
        //INIT TABLE AND COLUMNS
        taxiSquadTable = new TableView();
        taxiFirstNameColumn = new TableColumn(COL_FIRST_NAME);
        taxiLastNameColumn = new TableColumn(COL_LAST_NAME);
        taxiTeamColumn = new TableColumn(COL_TEAM);
        taxiPositionColumn = new TableColumn(COL_POSITION);
        taxiRorWColumn = new TableColumn(COL_R_OR_W);
        taxiHRorSVColumn = new TableColumn(COL_HR_OR_SV);
        taxiRBIorKColumn = new TableColumn(COL_RBI_OR_K);
        taxiSBorERAColumn = new TableColumn(COL_SB_OR_ERA);
        taxiBAorWHIPColumn = new TableColumn(COL_BA_OR_WHIP);
        taxiEstimatedValueColumn = new TableColumn(COL_ESTIMATED_VALUE);
        taxiContractColumn = new TableColumn(COL_CONTRACT);
        taxiSalaryColumn = new TableColumn(COL_SALARY);
        
        //LINK COLUMNS TO DATA
        taxiFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        taxiLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        taxiTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamMLB"));
        taxiPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        taxiRorWColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("R"));
        taxiHRorSVColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("HR"));
        taxiRBIorKColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("RBI"));
        taxiSBorERAColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("SB"));
        taxiBAorWHIPColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("BA"));
        taxiEstimatedValueColumn.setCellValueFactory(new PropertyValueFactory<String, String>("estimatedValue"));
        taxiContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        taxiSalaryColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("salary"));
        
        //ADD COLUMNS TO TABLE
        taxiSquadTable.getColumns().add(taxiPositionColumn);
        taxiSquadTable.getColumns().add(taxiFirstNameColumn);
        taxiSquadTable.getColumns().add(taxiLastNameColumn);
        taxiSquadTable.getColumns().add(taxiTeamColumn);
        taxiSquadTable.getColumns().add(taxiRorWColumn);
        taxiSquadTable.getColumns().add(taxiHRorSVColumn);
        taxiSquadTable.getColumns().add(taxiRBIorKColumn);
        taxiSquadTable.getColumns().add(taxiSBorERAColumn);
        taxiSquadTable.getColumns().add(taxiBAorWHIPColumn);
        taxiSquadTable.getColumns().add(taxiEstimatedValueColumn);
        taxiSquadTable.getColumns().add(taxiContractColumn);
        taxiSquadTable.getColumns().add(taxiSalaryColumn);
        taxiSquadTable.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
        
        //ADD HEADING TO TAXI SQUAD BOX
        taxiSquadBox = new VBox();
        taxiSquadHeading = initChildLabel(taxiSquadBox, WB_PropertyType.TAXI_SQUAD_LABEL, CLASS_SUBHEADING_LABEL);
        //ADD TABLE
        taxiSquadBox.getChildren().add(taxiSquadTable);

        
    }
    
    // INIT A TEXT FIELD AND PUT IT IN A Pane
    private TextField initTextField(Pane container, int size, boolean editable) {
        TextField tf = new TextField();
        tf.setPrefColumnCount(size);
        tf.setEditable(editable);
        container.getChildren().add(tf);
        return tf;
    }
    
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, WB_PropertyType icon, WB_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    //INIT EVENT HANDLERS FOR THIS GUI
    private void initEventHandlers(){
        //HANDLE TOOLBAR CONTROLS
        Draft draft = dataManager.getDraft();
        editController = new FantasyTeamEditController(primaryStage, yesNoCancelDialog);
        plusButton.setOnAction(e -> {
            editController.handleAddFantasyTeamRequest(switchScreenGui);
            setDataManager(switchScreenGui.getDataManager());
            selectTeamComboBox.setItems(dataManager.getDraft().getTeamNamesList());
            selectTeamComboBox.setValue(dataManager.getDraft().getTeamNamesList().get(dataManager.getDraft().getTeamNamesList().size() - 1));
        });
        minusButton.setOnAction(e -> {
            /*if(draft.getTeamNamesList().indexOf(selectTeamComboBox.getValue().toString()) == 0){
                updatePlayersTable = false;
            }*/
            editController.handleRemoveFantasyTeamRequest(switchScreenGui, dataManager.getDraft().getFantasyTeamMappings().get(selectTeamComboBox.getValue().toString()), this, updatePlayersTable);
            setDataManager(switchScreenGui.getDataManager());
            selectTeamComboBox.setItems(dataManager.getDraft().getTeamNamesList());
            
            if(dataManager.getDraft().getTeamNamesList().isEmpty()){
                selectTeamComboBox.setValue("");
            }
        });
        editButton.setOnAction(e -> {
            setDataManager(switchScreenGui.getDataManager());
            editController.handleEditFantasyTeamRequest(switchScreenGui, this, dataManager.getDraft().getFantasyTeamMappings().get(selectTeamComboBox.getValue().toString()));
        });
        selectTeamComboBox.setOnAction(e -> {
            ObservableList emptyList = FXCollections.observableArrayList();
            if(!draft.getTeamNamesList().isEmpty()){
                playersTable.setItems(switchScreenGui.getDataManager().getDraft().getFantasyTeamMappings().get(selectTeamComboBox.getValue().toString()).getVisibleStartingLineUp());
                taxiSquadTable.setItems(switchScreenGui.getDataManager().getDraft().getFantasyTeamMappings().get(selectTeamComboBox.getValue().toString()).getTaxiSquad());
            }
            else{
                playersTable.setItems(emptyList);
                taxiSquadTable.setItems(emptyList);
            }
                        
        });
        
        
        //HANDLE PLAYER TABLE CONTROLS
        playersTable.setOnMouseClicked(e -> {
            if(e.getClickCount() == 2){
                if(playersTable.getSelectionModel().getSelectedItem() instanceof Hitter){
                    Hitter h = (Hitter)playersTable.getSelectionModel().getSelectedItem();
                    playerEditController = new PlayerEditController(switchScreenGui.getPrimaryStage(), yesNoCancelDialog, messageDialog);
                    playerEditController.handleEditPlayerRequest(switchScreenGui, playersScreenGui, h);
                }
                else{
                    Pitcher p = (Pitcher)playersTable.getSelectionModel().getSelectedItem();
                    playerEditController = new PlayerEditController(switchScreenGui.getPrimaryStage(), yesNoCancelDialog, messageDialog);
                    playerEditController.handleEditPlayerRequest(switchScreenGui, playersScreenGui, p);
                }
                
            }
        });

    }


    
    
    
    


}
