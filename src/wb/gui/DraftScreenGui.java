package wb.gui;

import java.io.IOException;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wb.WB_StartupConstants.PATH_CSS;
import wb.data.DraftDataManager;
import wb.WB_PropertyType;
import static wb.WB_StartupConstants.PATH_IMAGES;
import wb.controller.DraftScreenController;
import wb.data.Player;
import static wb.gui.FantasyTeamsGui.COL_CONTRACT;
import static wb.gui.FantasyTeamsGui.COL_SALARY;
import static wb.gui.PlayersScreenGui.CLASS_RADIO_BUTTON_TOOLBAR;
import static wb.gui.PlayersScreenGui.COL_FIRST_NAME;
import static wb.gui.PlayersScreenGui.COL_LAST_NAME;


/**
 * This class provides the Graphical User Interface for the draft screen,
 * and displays the fantasy teams in the draft, along with their players
 * 
 * @author Awaes Choudhary
 */
public class DraftScreenGui {
    
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String COL_DRAFT_NUMBER = "Pick #";
    static final String COL_FANTASY_TEAM_NAME = "Team";
    
    //MANAGES ALL OF THE APPLICATIONS DATA
    DraftDataManager dataManager;
    
    //CONTAINS THE CONTENTS OF THE PAGE
    VBox draftScreenBox;
    
    //PAGE HEADING
    Label draftScreenLabel;
    
    //GUI COMPONENTS
    HBox toolbar;
    Button draftPlayerButton;
    Button startAutomatedDraftButton;
    Button pauseAutomatedDraftButton;
    VBox draftedPlayersTableBox;
    TableView<Player> draftedPlayersTable;
    TableColumn draftNumberColumn;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn fantasyTeamNameColumn;
    TableColumn contractColumn;
    TableColumn salaryColumn;
    
    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initSwitchScreenGui.
     *
     * @param initDataManager data manager for gui.**/
    public DraftScreenGui(DraftDataManager initDataManager){
        dataManager = initDataManager;
    }
    
    /**
     * Accessor method for the data manager.
     *
     * @return The DraftDataManager used by this UI.
     */
    public DraftDataManager getDataManager() {
        return dataManager;
    }
    
    /**
     * Accessor method for the fantasy teams pane.
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Pane getDraftScreenBox() {
        return draftScreenBox;
    }
    
    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The CourseDataManager to be used by this UI.
     */
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }
    
    /**
     * This method fully initializes the user interface for use.
     *
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initDraftScreenGui(){
        //INIT THE FANTASY TEAMS BOX
        initDraftScreenBox();
        
        //INIT EVENT HANDLERS
        initEventHandlers();
        
    }
    
     /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/

    
    //INITIALIZES THE FANTASY TEAMS BOX
    private void initDraftScreenBox(){
        draftScreenBox = new VBox();
        draftScreenLabel = initChildLabel(draftScreenBox, WB_PropertyType.DRAFT_LABEL, CLASS_HEADING_LABEL);
        //ADD DRAFTED PLAYERS BOX
        initDraftedPlayersTableBox();
        draftScreenBox.getChildren().add(draftedPlayersTableBox);
        
    }
    
    // INIT A LABEL AND PUT IT IN A TOOLBAR
    private Label initChildLabel(Pane container, WB_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(WB_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, WB_PropertyType icon, WB_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    private void initToolbar(){
        toolbar = new HBox();
        draftPlayerButton = initChildButton(toolbar, WB_PropertyType.DRAFT_PLAYER_ICON, WB_PropertyType.DRAFT_PLAYER_TOOLTIP, false);
        startAutomatedDraftButton = initChildButton(toolbar, WB_PropertyType.START_DRAFT_ICON, WB_PropertyType.START_DRAFT_TOOLTIP, false);
        pauseAutomatedDraftButton = initChildButton(toolbar, WB_PropertyType.PAUSE_DRAFT_ICON, WB_PropertyType.PAUSE_DRAFT_TOOLTIP, false);
    }
    
    private void initDraftedPlayersTableBox(){
        draftedPlayersTable = new TableView();
        draftNumberColumn = new TableColumn(COL_DRAFT_NUMBER);
        firstNameColumn = new TableColumn(COL_FIRST_NAME);
        lastNameColumn = new TableColumn(COL_LAST_NAME);
        fantasyTeamNameColumn = new TableColumn(COL_FANTASY_TEAM_NAME);
        contractColumn = new TableColumn(COL_CONTRACT);
        salaryColumn = new TableColumn(COL_SALARY);
        
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        contractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        salaryColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("salary"));
        fantasyTeamNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("fantasyTeamName"));
        draftNumberColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("draftNumber"));
        
        draftedPlayersTable.getColumns().add(draftNumberColumn);
        draftedPlayersTable.getColumns().add(firstNameColumn);
        draftedPlayersTable.getColumns().add(lastNameColumn);
        draftedPlayersTable.getColumns().add(fantasyTeamNameColumn);
        draftedPlayersTable.getColumns().add(contractColumn);
        draftedPlayersTable.getColumns().add(salaryColumn);
        draftedPlayersTable.setItems(dataManager.getDraft().getDraftedPlayers());
        
        //ADD ALL COMPONENTS TO DRAFTED PLAYERS TABLE BOX
        draftedPlayersTableBox = new VBox();
        //INIT AND ADD TOOLBAR FIRST
        initToolbar();
        draftedPlayersTableBox.getChildren().add(toolbar);
        //NOW ADD THE TABLE
        draftedPlayersTableBox.getChildren().add(draftedPlayersTable);
        draftedPlayersTableBox.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
    }
    
    private void initEventHandlers(){
        DraftScreenController controller = new DraftScreenController(dataManager);
        draftPlayerButton.setOnAction( e ->{
            controller.handleDraftPlayerRequest();
        });
        startAutomatedDraftButton.setOnAction( e -> {
            controller.handleStartAutomatedDraftRequest();
        });
        pauseAutomatedDraftButton.setOnAction( e-> {
            controller.handlePauseAutomatedDraft();
        });
    }
    
    


}
