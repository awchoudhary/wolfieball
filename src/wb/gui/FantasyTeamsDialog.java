package wb.gui;

import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wb.data.FantasyTeam;
import static wb.gui.PlayersScreenGui.CLASS_PROMPT_LABEL;
import static wb.gui.SwitchScreenGui.CLASS_HEADING_LABEL;
import static wb.gui.SwitchScreenGui.PRIMARY_STYLE_SHEET;

/**
 *
 * @author Awaes
 */
public class FantasyTeamsDialog extends Stage{
    //OBJECT DATA BEHIND UI
    FantasyTeam team = new FantasyTeam();
    
    //GUI CONTRLS
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label nameLabel;
    TextField nameTextField;
    Label ownerLabel;
    TextField ownerNameTextField;
    Button completeButton;
    Button cancelButton;
    
    //keeps track of which button was pressed
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String NAME_PROMPT = "Name:";
    public static final String OWNER_PROMPT = "Owner:";
    public static final String DIALOG_HEADING = "Fantasy Team Details";
    public static final String ADD_FANTASY_TEAM_TITLE = "Add New Fantasy Team";
    public static final String EDIT_FANTASY_TEAM_TITLE = "Edit Fantasy Team";
    
    /**
     * Initializes this dialog so that it can be used for either adding
     * new Fantasy Teams or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */

    public FantasyTeamsDialog(Stage primaryStage) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(DIALOG_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        // NOW THE NAME
        nameLabel = new Label(NAME_PROMPT);
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        nameTextField = new TextField();
        nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            team.setName(newValue);
        });
        
        // NOW THE OWNER
        ownerLabel = new Label(OWNER_PROMPT);
        ownerLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        ownerNameTextField = new TextField();
        ownerNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            team.setOwnerName(newValue);
        });

                
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            FantasyTeamsDialog.this.selection = sourceButton.getText();
            FantasyTeamsDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(nameLabel, 0, 1, 1, 1);
        gridPane.add(nameTextField, 1, 1, 1, 1);
        gridPane.add(ownerLabel, 0, 2, 1, 1);
        gridPane.add(ownerNameTextField, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);
        
        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public FantasyTeam getFantasyTeam() { 
        return team;
    }
    
    public FantasyTeam showAddFantasyTeamDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_FANTASY_TEAM_TITLE);
        
        // RESET THE SCHEDULE ITEM OBJECT WITH DEFAULT VALUES
        team = new FantasyTeam();
        
        // RESET GUI
        nameTextField.setText("");
        ownerNameTextField.setText("");
                
        // AND OPEN IT UP
        this.showAndWait();
        
        return team;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        nameTextField.setText(team.getName());
        ownerNameTextField.setText(team.getOwnerName());    
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditFantasyTeamDialog(FantasyTeam teamToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_FANTASY_TEAM_TITLE);
        
        // LOAD THE SCHEDULE ITEM INTO OUR LOCAL OBJECT
        team = new FantasyTeam();
        team.setName(teamToEdit.getName());
        team.setOwnerName(teamToEdit.getOwnerName());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }




}
