package wb.gui;

import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wb.WB_StartupConstants.PATH_CSS;
import wb.data.DraftDataManager;
import wb.WB_PropertyType;
import wb.data.Player;
import static wb.gui.PlayersScreenGui.CLASS_PROMPT_LABEL;
import static wb.gui.PlayersScreenGui.CLASS_RADIO_BUTTON_TOOLBAR;
import static wb.gui.PlayersScreenGui.COL_FIRST_NAME;
import static wb.gui.PlayersScreenGui.COL_LAST_NAME;
import static wb.gui.PlayersScreenGui.COL_POSITION;


/**
 * This class provides the Graphical User Interface for the mlb screen,
 * and displays the fantasy teams in the draft, along with their players
 * 
 * @author Awaes Choudhary
 */
public class MLBScreenGui {
    
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    
    //LIST OF ALL AVAILABLE PLAYERS
    ObservableList<Player> playersList;
    //VISIBLE LIST
    ObservableList<Player> visiblePlayersList = FXCollections.observableArrayList();
    
    //MANAGES ALL OF THE APPLICATIONS DATA
    DraftDataManager dataManager;
    
    //CONTAINS THE CONTENTS OF THE PAGE
    VBox MLBScreenBox;
    
    //PAGE HEADING
    Label MLBScreenLabel;
    
    //GUI CONTROLS
    VBox MLBTeamsTableBox;
    TableView<Player> MLBTeamsTable;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn QPColumn;
    HBox toolbar;
    Label selectMLBTeamLabel;
    ComboBox selectMLBTeamComboBox = new ComboBox();
    
    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initSwitchScreenGui.
     *
     * @param initDataManager data manager for gui.**/
    public MLBScreenGui(DraftDataManager initDataManager, ObservableList<Player> initPlayersList){
        dataManager = initDataManager;
        playersList = initPlayersList;
    }
    
    /**
     * Accessor method for the data manager.
     *
     * @return The DraftDataManager used by this UI.
     */
    public DraftDataManager getDataManager() {
        return dataManager;
    }
    
    /**
     * Accessor method for the fantasy teams pane.
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Pane getMLBScreenBox() {
        return MLBScreenBox;
    }
    
    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The CourseDataManager to be used by this UI.
     */
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }
    
    /**
     * This method fully initializes the user interface for use.
     *
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initMLBScreenGui(){
        //INIT EVENT HANDLERS
        initEventHandlers();
        //INIT THE FANTASY TEAMS BOX
        initMLBScreenBox();
        
    }
    
     /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/

    
    //INITIALIZES THE FANTASY TEAMS BOX
    private void initMLBScreenBox(){
        //ADD SCREEN HEADING
        MLBScreenBox = new VBox();
        initChildLabel(MLBScreenBox, WB_PropertyType.MLB_LABEL, CLASS_HEADING_LABEL);
        initMLBTeamTable();
        MLBScreenBox.getChildren().add(MLBTeamsTableBox);
        
    }
    
    // INIT A LABEL AND PUT IT IN A TOOLBAR
    private Label initChildLabel(Pane container, WB_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(WB_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    //INITIALIZE THE MLB TEAMS TABLE
    private void initMLBTeamTable(){
        //INIT AND ADD COMBOBOX AND LABEL TO MLB TEAMS BOX
        toolbar = new HBox();
        selectMLBTeamLabel = initChildLabel(toolbar, WB_PropertyType.SELECT_MLB_TEAM_LABEL, CLASS_PROMPT_LABEL);
        selectMLBTeamComboBox = fillSelectMLBTeamComboBox(selectMLBTeamComboBox);
        toolbar.getChildren().add(selectMLBTeamComboBox);
        toolbar.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
        //INIT TABLE
        MLBTeamsTable = new TableView();
        MLBTeamsTable.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
        //INIT COLUMNS
        firstNameColumn = new TableColumn(COL_FIRST_NAME);
        lastNameColumn = new TableColumn(COL_LAST_NAME);
        QPColumn = new TableColumn(COL_POSITION);
        //SET CELL FACTORY VALUES
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        QPColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        //ADD COLUMNS TO TABLES
        MLBTeamsTable.getColumns().add(firstNameColumn);
        MLBTeamsTable.getColumns().add(lastNameColumn);
        MLBTeamsTable.getColumns().add(QPColumn);
        //MLBTeamsTable.setItems(visiblePlayersList);
        MLBTeamsTableBox = new VBox();
        MLBTeamsTableBox.getChildren().add(toolbar);
        MLBTeamsTableBox.getChildren().add(MLBTeamsTable);
    }
    
    private ComboBox fillSelectMLBTeamComboBox(ComboBox comboBox){
        comboBox.getItems().add("ATL");
        comboBox.getItems().add("AZ");
        comboBox.getItems().add("CHC");
        comboBox.getItems().add("CIN");
        comboBox.getItems().add("COL");
        comboBox.getItems().add("LAD");
        comboBox.getItems().add("MIA");
        comboBox.getItems().add("MIL");
        comboBox.getItems().add("NYM");
        comboBox.getItems().add("PHI");
        comboBox.getItems().add("PIT");
        comboBox.getItems().add("SD");
        comboBox.getItems().add("SF");
        comboBox.getItems().add("STL");
        comboBox.getItems().add("WAS");
        return comboBox;
    }
    
    private void initEventHandlers(){
       selectMLBTeamComboBox.setOnAction(e -> {
           visiblePlayersList = filterForMLBTeam(selectMLBTeamComboBox.getValue().toString(), playersList);
           MLBTeamsTable.setItems(visiblePlayersList);
       });
    }
    
    //FILTERS A PLAYERS ARRAY FOR PLAYERS IN THE ARGUMENT MLB TEAM NAME
    private ObservableList<Player> filterForMLBTeam(String teamName, ObservableList<Player> srcList){
        ObservableList filteredList = FXCollections.observableArrayList();
        for(int i = 0; i < srcList.size(); i++){
            if(srcList.get(i).getTeamMLB().equals(teamName)){
                filteredList.add(srcList.get(i));
            }
        }
        return filteredList;
    }

    
    


}
