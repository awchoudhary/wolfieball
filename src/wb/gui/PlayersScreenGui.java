package wb.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import wb.WB_PropertyType;
import static wb.WB_StartupConstants.*;
import wb.controller.PlayerEditController;
import wb.data.DraftDataManager;
import wb.data.Hitter;
import wb.data.Pitcher;
import wb.data.Player;

/**
 * This class provides the Graphical User Interface for the players screen,
 * managing all the UI components for sorting undrafted players and displaying
 * them.
 *
 * @author Awaes Choudhary
 */
public class PlayersScreenGui {
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String CLASS_PLAYERS_SCREEN = "players_screen";
    static final String CLASS_RADIO_BUTTON_TOOLBAR = "radio_button_toolbar";
    static final int SEARCH_BAR_LENGTH = 50;
    
    //LIST THAT WILL BE LINKED TO TABLE
    ObservableList<Player> visiblePlayers;
    
    //LIST THAT WILL CONTAIN ALL PLAYERS
    //AND WILL ACT AS A REFERENCE
    ObservableList<Player> playersList;
    
    //REFERENCE LIST USED TO IMPLEMENT SEARCH BAR
    ObservableList referenceList = FXCollections.observableArrayList();
    boolean updateReferenceList = true;
    
    //MESSAGE DIALOGS
    YesNoCancelDialog yesNoCancelDialog;
    MessageDialog messageDialog;
    
    //MANAGES ALL OF THE APPLICATIONS DATA
    DraftDataManager dataManager;
    
    //EDIT CONTROLLER FOR ADD REMOVE AND EDIT
    PlayerEditController editController;
    
    //PRIMARY GUI
    SwitchScreenGui switchScreenGui;
    
    //CONTAINS THE CONTENTS OF THE PAGE
    VBox playersBox;
    
    //HEADING FOR PAGE
    Label playersLabel;
    
    //CONTROLS FOR TOP TOOLBAR
    HBox playersToolbar;
    Label searchBarLabel;
    Button addButton;
    Button removeButton;
    TextField searchBar;
    
    //CONTROLS FOR RADIO BUTTONS USED FOR SORTING
    HBox radioButtonsToolbar;
    RadioButton radioButtonAll;
    RadioButton radioButtonC;
    RadioButton radioButton1B;
    RadioButton radioButtonCI;
    RadioButton radioButton3B;
    RadioButton radioButton2B;
    RadioButton radioButtonMI;
    RadioButton radioButtonSS;
    RadioButton radioButtonOF;
    RadioButton radioButtonU;
    RadioButton radioButtonP;
    ToggleGroup group = new ToggleGroup();
    //CONTROLS FOR THE PLAYERS TABLE
    VBox playersTableBox;
    TableView<Player> playersTable;
    TableColumn firstNameColumn;
    TableColumn lastNameColumn;
    TableColumn teamColumn;
    TableColumn positionColumn;
    TableColumn yearOfBirthColumn;
    TableColumn RorWColumn;
    TableColumn HRorSVColumn;
    TableColumn RBIorKColumn;
    TableColumn SBorERAColumn;
    TableColumn BAorWHIPColumn;
    TableColumn estimatedValueColumn;
    TableColumn notesColumn;
    
    static final String COL_FIRST_NAME = "First Name";
    static final String COL_LAST_NAME = "Last Name";
    static final String COL_TEAM = "Pro Team";
    static final String COL_POSITION = "Positions";
    static final String COL_YEAR_OF_BIRTH = "Year of Birth";
    static final String COL_R_OR_W = "R/W";
    static final String COL_HR_OR_SV = "HR/SV";
    static final String COL_RBI_OR_K = "RBI/K";
    static final String COL_SB_OR_ERA = "SB/ERA";
    static final String COL_BA_OR_WHIP = "BA/WHIP";
    static final String COL_ESTIMATED_VALUE = "Estimated Value";
    static final String COL_NOTES = "Notes";
    
    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initPlayersScreenGui.
     *
     * @param initDataManager : Data manager to be used by this gui
     */
    public PlayersScreenGui(DraftDataManager initDataManager, SwitchScreenGui initSwitchScreenGui){
        dataManager = initDataManager;
        visiblePlayers = FXCollections.observableArrayList();
        switchScreenGui = initSwitchScreenGui;
        playersList = FXCollections.observableArrayList();
    }
    
    /**
     * Accessor method for the data manager.
     *
     * @return The DraftDataManager used by this UI.
     */
    public DraftDataManager getDataManager() {
        return dataManager;
    }
    
    /**
     * Accessor method for the players box pane.
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Pane getPlayersBox() {
        return playersBox;
    }
    
    public ObservableList<Player> getVisiblePlayersList(){
        return visiblePlayers;
    }
    
    public ObservableList<Player> getPlayersList(){
        return playersList;
    }
    
    public ObservableList<Player> getReferenceList(){
        return referenceList;
    }

    
    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The DraftDataManager to be used by this UI.
     */
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }
    
    /**
     * This method fully initializes the user interface for use.
     *
     * */
    public void initPlayersScreenGui(){
        //INITIALIZE THE PLAYERS LIST TO
        //LIST OF DRAFT BEING EDITED
        playersList = dataManager.getDraft().getPlayersList();
        //INIT THE PLAYERS TOOLBAR
        initPlayersToolbar();
        
        //INIT THE RADIO BUTTONS TOOLBAR
        initRadioButtonsToolbar();
        
        
        //INIT PLAYERS TABLE
        initPlayersTable();
        
        //INIT MESSAGE DIALOGS
        initMessageDialogs();
        
        //INIT THE EVENT HANDLERS FOR CONTROLS
        initEventHandlers();

        
        //ADD THEM TO PLAYERS BOX
        playersBox = new VBox();
        playersLabel = initChildLabel(playersBox, WB_PropertyType.PLAYERS_LABEL, CLASS_HEADING_LABEL);
        playersBox.getChildren().add(playersToolbar);
        playersBox.getChildren().add(radioButtonsToolbar);
        playersBox.getChildren().add(playersTable);
        playersBox.getStyleClass().add(CLASS_PLAYERS_SCREEN);
    }

    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/
    //INIT MESSAGE DIALOGS
    private void initMessageDialogs(){
        yesNoCancelDialog = new YesNoCancelDialog(switchScreenGui.getPrimaryStage());
        messageDialog = new MessageDialog(switchScreenGui.getPrimaryStage(), CLOSE_BUTTON_LABEL);
    }
    //INIT THE PLAYERS TOOLBAR
    private void initPlayersToolbar(){
        playersToolbar = new HBox();
        addButton = initChildButton(playersToolbar, WB_PropertyType.ADD_ICON, WB_PropertyType.ADD_PLAYER_TOOLTIP, false);
        removeButton = initChildButton(playersToolbar, WB_PropertyType.MINUS_ICON, WB_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
        searchBarLabel = initChildLabel(playersToolbar, WB_PropertyType.SEARCH_BAR_LABEL, CLASS_PROMPT_LABEL);
        searchBar = initTextField(playersToolbar, SEARCH_BAR_LENGTH, true);
        playersToolbar.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
    }
    
    //INIT THE RADIO BUTTONS TOOLBAR
    private void initRadioButtonsToolbar(){
        radioButtonsToolbar = new HBox();
        radioButtonAll = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_ALL_LABEL, true, group);
        radioButtonC = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_C_LABEL, false, group);
        radioButton1B = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_1B_LABEL, false, group);
        radioButtonCI = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_CI_LABEL, false, group);
        radioButton3B = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_3B_LABEL, false, group);
        radioButton2B = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_2B_LABEL, false, group);
        radioButtonMI = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_MI_LABEL, false, group);
        radioButtonSS = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_SS_LABEL, false, group);
        radioButtonOF = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_OF_LABEL, false, group);
        radioButtonU = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_U_LABEL, false, group);
        radioButtonP = initRadioButton(radioButtonsToolbar, WB_PropertyType.RADIO_P_LABEL, false, group);
        radioButtonsToolbar.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
    }
    
    // INIT A LABEL AND PUT IT IN A TOOLBAR
    private Label initChildLabel(Pane container, WB_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(WB_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, WB_PropertyType icon, WB_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    // INIT A TEXT FIELD AND PUT IT IN A Pane
    private TextField initTextField(Pane container, int size, boolean editable) {
        TextField tf = new TextField();
        tf.setPrefColumnCount(size);
        tf.setEditable(editable);
        container.getChildren().add(tf);
        return tf;
    }
    
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private RadioButton initRadioButton(Pane toolbar, WB_PropertyType label, boolean selected, ToggleGroup group) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        RadioButton button = new RadioButton(props.getProperty(label.toString()));
        button.setToggleGroup(group);
        toolbar.getChildren().add(button);
        button.setSelected(selected);
        return button;
    }
    
    private void initPlayersTable(){
        //INIT THE PLAYERS TABLE
        playersTable = new TableView();
        playersTable.setEditable(true);
        clonePlayersList(playersList, visiblePlayers);
        //INIT THE PLAYERS TABLE COMLUMN
        firstNameColumn = new TableColumn(COL_FIRST_NAME);
        lastNameColumn = new TableColumn(COL_LAST_NAME);
        teamColumn = new TableColumn(COL_TEAM);
        positionColumn = new TableColumn(COL_POSITION);
        yearOfBirthColumn = new TableColumn(COL_YEAR_OF_BIRTH);
        RorWColumn = new TableColumn(COL_R_OR_W);
        HRorSVColumn = new TableColumn(COL_HR_OR_SV);
        RBIorKColumn = new TableColumn(COL_RBI_OR_K);
        SBorERAColumn = new TableColumn(COL_SB_OR_ERA);
        BAorWHIPColumn = new TableColumn(COL_BA_OR_WHIP);
        estimatedValueColumn = new TableColumn(COL_ESTIMATED_VALUE);
        notesColumn = new TableColumn(COL_NOTES);
        
                
        
        
        //AND LINK THE COLUMNS TO THE DATA
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        teamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamMLB"));
        positionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        yearOfBirthColumn.setCellValueFactory(new PropertyValueFactory<String, String>("yearOfBirth"));
        RorWColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("R"));
        HRorSVColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("HR"));
        RBIorKColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("RBI"));
        SBorERAColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("SB"));
        BAorWHIPColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("BA"));
        estimatedValueColumn.setCellValueFactory(new PropertyValueFactory<String, String>("estimatedValue"));
        notesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));
        //set the notes column to be editable
        notesColumn.setEditable(true);
        notesColumn.setCellFactory(TextFieldTableCell.<String>forTableColumn());
        notesColumn.setOnEditCommit( e -> {
            new EventHandler<TableColumn.CellEditEvent<Player, String>>() {
               @Override
               public void handle(TableColumn.CellEditEvent<Player, String> t) {
                ((Player)t.getTableView().getItems().get(t.getTablePosition().getRow())).setNotes(t.getNewValue());
               }
            };
        });
        
        //ADD COLUMNS TO TABLE
        playersTable.getColumns().add(firstNameColumn);
        playersTable.getColumns().add(lastNameColumn);
        playersTable.getColumns().add(teamColumn);
        playersTable.getColumns().add(positionColumn);
        playersTable.getColumns().add(yearOfBirthColumn);
        playersTable.getColumns().add(RorWColumn);
        playersTable.getColumns().add(HRorSVColumn);
        playersTable.getColumns().add(RBIorKColumn);
        playersTable.getColumns().add(SBorERAColumn);
        playersTable.getColumns().add(BAorWHIPColumn);
        playersTable.getColumns().add(estimatedValueColumn);
        playersTable.getColumns().add(notesColumn);
        playersTable.setItems(visiblePlayers);
        playersTable.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
        
    }
    
    //INITIALIZE THE CONTROLS FOR THE PAGE
    private void initEventHandlers(){
        //EVENT HANDLERS FOR THE RADIO BUTTONS
        radioButtonAll.setOnAction(e -> {
            clonePlayersList(playersList, visiblePlayers);
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());
        });
        radioButtonC.setOnAction(e -> {
            filterArrayForOnePosition(visiblePlayers, "C_");
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());
        });
        radioButton1B.setOnAction(e -> {
            filterArrayForOnePosition(visiblePlayers, "1B");
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());

        });
        radioButtonCI.setOnAction(e -> {
            filterArrayForTwoPositions(visiblePlayers, "1B", "3B");
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());

        });
        radioButton3B.setOnAction(e -> {
            filterArrayForOnePosition(visiblePlayers, "3B");
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());

        });
        radioButton2B.setOnAction(e -> {
            filterArrayForOnePosition(visiblePlayers, "2B");
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());

        });
        radioButtonMI.setOnAction(e -> {
            filterArrayForTwoPositions(visiblePlayers, "2B", "SS");
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());

        });
        radioButtonSS.setOnAction(e -> {
            filterArrayForOnePosition(visiblePlayers, "SS");
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());

        });
        radioButtonOF.setOnAction(e -> {
            filterArrayForOnePosition(visiblePlayers, "OF");
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());

        });
        radioButtonU.setOnAction(e -> {
            filterArrayForHitters(visiblePlayers);
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());

        });
        radioButtonP.setOnAction(e -> {
            filterArrayForOnePosition(visiblePlayers, "P");
            updateReferenceList = true;
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());

        });
        
        //EVENT HANDLER FOR THE SEARCH BAR
        searchBar.setOnKeyReleased(e -> {
            //create an arraylist to use as a reference
            filterArrayForText(visiblePlayers, referenceList, searchBar.getText());
        });
        
        //EVENT HANDLER FOR ADD AND REMOVE PLAYER
        addButton.setOnAction(e -> {
            editController = new PlayerEditController(switchScreenGui.getPrimaryStage(), yesNoCancelDialog, messageDialog);
            editController.handleAddPlayerRequest(switchScreenGui, this);
        });
        removeButton.setOnAction(e -> {
            editController = new PlayerEditController(switchScreenGui.getPrimaryStage(), yesNoCancelDialog, messageDialog);
            editController.handleRemovePlayerRequest(this, playersList.get(getIndexOfPlayer(playersList, playersTable.getSelectionModel().getSelectedItem())));
        });
        //EVENT HANDLER FOR EDIT PLAYER
        playersTable.setOnMouseClicked(e -> {
            if(e.getClickCount() == 2){
                if(playersTable.getSelectionModel().getSelectedItem() instanceof Hitter){
                    Hitter h = (Hitter)playersTable.getSelectionModel().getSelectedItem();
                    h = (Hitter)playersList.get(getIndexOfPlayer(playersList, h));
                    editController = new PlayerEditController(switchScreenGui.getPrimaryStage(), yesNoCancelDialog, messageDialog);
                    editController.handleEditPlayerRequest(switchScreenGui, this, h);
                }
                else{
                    Pitcher p = (Pitcher)playersTable.getSelectionModel().getSelectedItem();
                    p = (Pitcher)playersList.get(getIndexOfPlayer(playersList, p));
                    editController = new PlayerEditController(switchScreenGui.getPrimaryStage(), yesNoCancelDialog, messageDialog);
                    editController.handleEditPlayerRequest(switchScreenGui, this, p);
                }
                
            }
        });
    }
    
    //COPIES THE SRC ARRAY INTO THE DST ARRAY
    private void clonePlayersList(ObservableList<Player> src, ObservableList<Player> dst){
        //CLEAR THE DST ARRAY FIRST
        dst.clear();
        for(int i = 0; i < src.size(); i++){
            if(src.get(i).getFantasyTeamName().equals("Free Agent")){
                if(src.get(i) instanceof Hitter){
                    Hitter h = new Hitter();
                    ((Hitter)src.get(i)).clone(h);
                    dst.add(h);
                }
                else{
                    Pitcher p = new Pitcher();
                    ((Pitcher)src.get(i)).clone(p);
                    dst.add(p);
                }
            }
        }
    }
    
    //CREATES AN ARRAY TO CONTAIN PLAYERS WITH ONLY
    //THE SPECIFIED POSITION BY ITERATING THROUGH THE PLAYERS LIST
    private void filterArrayForOnePosition(ObservableList<Player> list, String position){
        //empty list first
        list.clear();
        //look through array and remove players that do not
        //fit the specified position
        for(int i = 0; i < playersList.size(); i++){
            if(playersList.get(i).getFantasyTeamName().equals("Free Agent")){
                if(playersList.get(i) instanceof Hitter){
                    if(((((Hitter)playersList.get(i)).getQP()).contains(position))){
                        Hitter h = new Hitter();
                        ((Hitter)(playersList.get(i))).clone(h);
                        list.add(h);
                    }
                }
                else{
                    if(((((Pitcher)playersList.get(i)).getRole()).contains(position))){
                        Pitcher p = new Pitcher();
                        ((Pitcher)playersList.get(i)).clone(p);
                        list.add(p);
                    }
                }
            }
        }
        
    }
    
    //FILTERS ARRAY FOR TWO POSITIONS
    private void filterArrayForTwoPositions(ObservableList<Player> list, String position1, String position2){
        //empty list first
        list.clear();
        //look through array and remove players that do not
        //fit the specified position
        for(int i = 0; i < playersList.size(); i++){
            if(playersList.get(i).getFantasyTeamName().equals("Free Agent")){
                if(playersList.get(i) instanceof Hitter){
                    if(((((Hitter)playersList.get(i)).getQP()).contains(position1)) || ((((Hitter)playersList.get(i)).getQP()).contains(position2))){
                        Hitter h = new Hitter();
                        ((Hitter)(playersList.get(i))).clone(h);
                        list.add(h);
                    }
                }
                else{
                    if(((((Pitcher)playersList.get(i)).getRole()).contains(position1)) || ((((Pitcher)playersList.get(i)).getRole()).contains(position2))){
                        Pitcher p = new Pitcher();
                        ((Pitcher)playersList.get(i)).clone(p);
                        list.add(p);
                    }
                }
            }
        }

    }
    
    //FILTERS ARRAY FOR JUST HITTERS
    private void filterArrayForHitters(ObservableList<Player> list){
        //empty list first
        list.clear();
        //look through array and remove players that do not
        //fit the specified position
        for(int i = 0; i < playersList.size(); i++){
            if(playersList.get(i).getFantasyTeamName().equals("Free Agent")){
                if(playersList.get(i) instanceof Hitter){
                    Hitter h = new Hitter();
                    ((Hitter)(playersList.get(i))).clone(h);
                    list.add(h);

                }
            }
        }

    }
    
    //CREATES AN ARRAY TO CONTAIN PLAYERS WITH ONLY
    //THE SPECIFIED TEXT IN THEIR FIRST OR LAST NAME 
    //BY ITERATING THROUGH THE PLAYERS LIST
    private void filterArrayForText(ObservableList<Player> list, ObservableList<Player> referenceList, String text){
        //update array according to updateReferenceList
        if(updateReferenceList){
            clonePlayersList(visiblePlayers, referenceList);
        }
        updateReferenceList = false;
        //clear list first
        list.clear();
        //look through array and add players that do 
        //fit the specified text
        for(int i = 0; i < referenceList.size(); i++){
            if(referenceList.get(i).getFirstName().startsWith(text) || referenceList.get(i).getLastName().startsWith(text)){
                if(referenceList.get(i) instanceof Hitter){
                    Hitter h = new Hitter();
                    ((Hitter)referenceList.get(i)).clone(h);
                    list.add(h);
                }
                else{
                    Pitcher p = new Pitcher();
                    ((Pitcher)referenceList.get(i)).clone(p);
                    list.add(p);
                }
            }
        }
        
    }
    
    private int getIndexOfPlayer(ObservableList<Player> list, Player p){
        for(int i = 0; i < list.size(); i++){
            if(p.equals(list.get(i))){
                return i;
            }
        }
        return 0;
    }
}
