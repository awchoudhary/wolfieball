package wb.gui;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import static wb.WB_StartupConstants.*;
import wb.WB_PropertyType;
import wb.data.DraftDataManager;
import wb.file.DraftFileManager;
import wb.controller.FileController;
import properties_manager.PropertiesManager;
import wb.data.Draft;
import wb.data.DraftDataView;

/**
 * This class provides the Graphical User Interface for this application,
 * managing all the UI components for editing a Draft and exporting its details.
 *
 * @author Awaes Choudhary
 */
public class SwitchScreenGui implements DraftDataView{
    
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_WORK_SPACE = "work_space";
    static final int SEARCH_BAR_LENGTH = 50;
    
    //THIS MANAGES DRAFT FILE INPUT/OUTPUT
    DraftFileManager fileManager;

    //MANAGES ALL OF THE APPLICATIONS DATA
    DraftDataManager dataManager;
    
    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    FileController fileController;
    
    // THIS IS THE APPLICATION WINDOW
    Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    Scene primaryScene;
    
    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane wbPane;
    
    //ALL THE SCREENS USED IN THE GUI
    FantasyTeamsGui fantasyTeamsGui;
    FantasyTeamsStandingsGui fantasyTeamsStandingsGui;
    PlayersScreenGui playersScreenGui;
    DraftScreenGui draftScreenGui;
    MLBScreenGui MLBScreenGui;
    
    // WE'LL ORGANIZE OUR WORKSPACE COMPONENTS USING A BORDER PANE
    BorderPane workspacePane;
    boolean workspaceActivated;
    
    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newDraftButton;
    Button loadDraftButton;
    Button saveDraftButton;
    Button exportSiteButton;
    Button exitButton;
    
    //THIS IS THE BOTTOM TOOLABR AND ITS CONTROLS
    FlowPane screenToolbarPane;
    Button fantasyTeamsButton;
    Button playersButton;
    Button fantasyTeamsStandingsButton;
    Button draftButton;
    Button MLBTeamsButton;
    
    //DIALOGS TO BE USED
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initSwitchScreenGui.
     *
     * @param initPrimaryStage Window inside which the GUI will be displayed.**/
    public SwitchScreenGui(Stage initPrimaryStage){
        primaryStage = initPrimaryStage;
    }
    
    /**
     * Accessor method for the data manager.
     *
     * @return The DraftDataManager used by this UI.
     */
    public DraftDataManager getDataManager() {
        return dataManager;
    }
    
    /**
     * Accessor method for the file controller.
     *
     * @return The FileController used by this UI.
     */
    public FileController getFileController() {
        return fileController;
    }
    
    /**
     * Accessor method for the draft file manager.
     *
     * @return The DraftFileManager used by this UI.
     */
    public DraftFileManager getDraftFileManager() {
        return fileManager;
    }
    
    /**
     * Accessor method for the window (i.e. stage).
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Stage getWindow() {
        return primaryStage;
    }
    
    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The CourseDataManager to be used by this UI.
     */
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }
    
    public TextField getDraftNameTextField(){
        return fantasyTeamsGui.getDraftNameTextField();
    }
    
    /**
     * Mutator method for the draft file manager.
     *
     * @param initDraftFileManager The DraftFileManager to be used by this UI.
     */
    public void setDraftFileManager(DraftFileManager initDraftFileManager) {
        fileManager = initDraftFileManager;
    }
    
    public Stage getPrimaryStage(){
        return primaryStage;
    }
    
    /**
     * This method fully initializes the user interface for use.
     *
     * @param windowTitle The text to appear in the UI window's title bar.
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initSwitchScreenGui(String windowTitle) throws IOException {
        
        // INIT THE DIALOGS
        initDialogs();

        // INIT THE FILETOOLBAR
        initFileToolbar();
        
        // INIT THE SCREENTOOLBAR
        initScreenToolbar();
        
        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();
        
        //INIT EVENT HANDLERS
        initEventHandlers();
        
        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }
    
    /**
     * When called this function puts the workspace into the window,
     * revealing the controls for editing a Draft.
     */
    public void activateWorkspace() {
        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            workspacePane.setCenter(fantasyTeamsGui.getFantasyTeamsBox());
            wbPane.setCenter(workspacePane);
            //workspaceActivated = true;
        }
    }
    
    @Override
    public void reloadDraft(Draft draftToReload){
        if(!workspaceActivated){
            activateWorkspace();
        }
    }
    
    /**
     * This method is used to activate/deactivate toolbar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * 
     * @param saved Describes whether the loaded Course has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveDraftButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        loadDraftButton.setDisable(false);
        exportSiteButton.setDisable(false);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }

    
    

    
    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/
    
    private void initDialogs(){
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
    }
    
    private void initFileToolbar(){
        fileToolbarPane = new FlowPane();
        
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newDraftButton = initChildButton(fileToolbarPane, WB_PropertyType.NEW_DRAFT_ICON, WB_PropertyType.NEW_DRAFT_TOOLTIP, false);
        loadDraftButton = initChildButton(fileToolbarPane, WB_PropertyType.LOAD_DRAFT_ICON, WB_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        saveDraftButton = initChildButton(fileToolbarPane, WB_PropertyType.SAVE_DRAFT_ICON, WB_PropertyType.SAVE_DRAFT_TOOLTIP, true);
        exportSiteButton = initChildButton(fileToolbarPane, WB_PropertyType.EXPORT_PAGE_ICON, WB_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, WB_PropertyType.EXIT_ICON, WB_PropertyType.EXIT_TOOLTIP, true);
    }
    
    private void initScreenToolbar(){
        screenToolbarPane = new FlowPane();
        
        //HERE ARE THE SCREEN TOOLBAR BUTTONS
        //THE BUTTONS WILL ALWAYS BE ENABLED
        fantasyTeamsButton = initChildButton(screenToolbarPane, WB_PropertyType.FANTASY_TEAMS_ICON, WB_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        playersButton = initChildButton(screenToolbarPane, WB_PropertyType.PLAYERS_ICON, WB_PropertyType.PLAYERS_TOOLTIP, false);
        draftButton = initChildButton(screenToolbarPane, WB_PropertyType.DRAFT_ICON, WB_PropertyType.DRAFT_TOOLTIP, false);
        fantasyTeamsStandingsButton = initChildButton(screenToolbarPane, WB_PropertyType.FANTASY_TEAMS_STANDINGS_ICON, WB_PropertyType.FANTASY_TEAMS_STANDINGS_TOOLTIP, false);
        MLBTeamsButton = initChildButton(screenToolbarPane, WB_PropertyType.MLB_ICON, WB_PropertyType.MLB_TOOLTIP, false);
    }
    
    private void initWorkspace() throws IOException{
        
        //INIT THE FANTASY TEAMS GUI
        initFantasyTeamsGui();
        
        //INIT WORKSPACE PANE
        workspacePane = new BorderPane();
        workspacePane.getStyleClass().add(CLASS_WORK_SPACE);
        
        //SET WORKSPACE ACTIVATED TO FALSE
        workspaceActivated = false;

    }
    
    //INIT THE SCREENS USED IN THE APPLICATION
    private void initFantasyTeamsGui(){
        fantasyTeamsGui = new FantasyTeamsGui(primaryStage, this, dataManager);
        fantasyTeamsGui.initFantasyTeamsGui();
    }
    
    
    private void initFantasyTeamsStandingsGui(){
        fantasyTeamsStandingsGui = new FantasyTeamsStandingsGui(dataManager);
        fantasyTeamsStandingsGui.initFantasyTeamsStandingsGui();
    }
    
    private void initPlayersGui(){
        playersScreenGui = new PlayersScreenGui(dataManager, this);
        playersScreenGui.initPlayersScreenGui();
    }
    
    private void initDraftScreenGui(){
        draftScreenGui = new DraftScreenGui(dataManager);
        draftScreenGui.initDraftScreenGui();
    }
    
    private void initMLBScreenGui(){
        MLBScreenGui = new MLBScreenGui(dataManager, dataManager.getDraft().getPlayersList());
        MLBScreenGui.initMLBScreenGui();
    }




    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, WB_PropertyType icon, WB_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Draft IS CREATED OR LOADED
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        wbPane = new BorderPane();
        wbPane.setTop(fileToolbarPane);
        wbPane.setBottom(screenToolbarPane);
        primaryScene = new Scene(wbPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
    
    //INIT ALL THE EVENT HANDLERS
    private void initEventHandlers() throws IOException{
        //FIRST THE FILE CONTROLS
        fileController = new FileController(fileManager, messageDialog, yesNoCancelDialog);
        newDraftButton.setOnAction( e -> {
            fileController.handleNewDraftRequest(this);
        });
        saveDraftButton.setOnAction( e -> {
            fileController.handleSaveCourseRequest(this, dataManager.getDraft(), fantasyTeamsGui.getDraftName());
        });
        loadDraftButton.setOnAction( e -> {
            try {
                fileController.handleLoadCourseRequest(this);
            } catch (IOException ex) {
                Logger.getLogger(SwitchScreenGui.class.getName()).log(Level.SEVERE, null, ex);
            }
            activateWorkspace();
        });
        
        //THE SCREEN TOOLBAR CONTROLS
        fantasyTeamsButton.setOnAction( e -> {
            initFantasyTeamsGui();
            workspacePane.setCenter(fantasyTeamsGui.getFantasyTeamsBox());
        });
        fantasyTeamsStandingsButton.setOnAction( e -> {
            initFantasyTeamsStandingsGui();
            workspacePane.setCenter(fantasyTeamsStandingsGui.getFantasyTeamsStandingsBox());
        });
        playersButton.setOnAction( e -> {
            initPlayersGui();
            workspacePane.setCenter(playersScreenGui.getPlayersBox());
        });
        draftButton.setOnAction( e -> {
            initDraftScreenGui();
            workspacePane.setCenter(draftScreenGui.getDraftScreenBox());
        });
        MLBTeamsButton.setOnAction( e -> {
            initMLBScreenGui();
            workspacePane.setCenter(MLBScreenGui.getMLBScreenBox());
        });
    }



    
}
