package wb.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wb.WB_PropertyType;
import static wb.WB_StartupConstants.PATH_FLAGS_IMAGES;
import static wb.WB_StartupConstants.PATH_IMAGES;
import static wb.WB_StartupConstants.PATH_PLAYERS_IMAGES;
import wb.data.Draft;
import wb.data.FantasyTeam;
import wb.data.Hitter;
import wb.data.Pitcher;
import wb.data.Player;
import static wb.gui.PlayersScreenGui.CLASS_PROMPT_LABEL;
import static wb.gui.SwitchScreenGui.CLASS_HEADING_LABEL;
import static wb.gui.SwitchScreenGui.PRIMARY_STYLE_SHEET;


/**
 * GUI for player edit and remove dialog. 
 * @author Awaes
 */
public class EditPlayerDialog extends Stage{
    //DATA BEHIND UI
    Player player;
    
    MessageDialog messageDialog;
    
    Boolean addFreeAgent = true;
    
    //GUI CONTROL FOR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label fantasyTeamsLabel;
    ComboBox fantasyTeamsComboBox;
    Label positionLabel;
    ComboBox positionComboBox;
    Label contractLabel;
    ComboBox contractComboBox;
    Label salaryLabel;
    TextField salaryTextField;
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String HEADING_LABEL = "Player Details";
    public static final String FANTASY_TEAMS_LABEL = "Fantasy Team:";
    public static final String POSITION_LABEL = "Position:";
    public static final String CONTRACT_LABEL = "Contract:";
    public static final String SALARY_LABEL = "Salary($):";
    
    public EditPlayerDialog(Stage primaryStage, MessageDialog initMessageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        messageDialog = initMessageDialog;
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(HEADING_LABEL);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
    
        //FANTASY TEAM SELECTION COMBOBOX
        fantasyTeamsLabel = new Label(FANTASY_TEAMS_LABEL);
        fantasyTeamsComboBox = new ComboBox();
        fantasyTeamsComboBox.setOnAction(e -> {
            player.setFantasyTeamName(fantasyTeamsComboBox.getValue().toString());
        });

                
        //POSITION SELECTION COMBOBOX
        positionLabel = new Label(POSITION_LABEL);
        positionComboBox = new ComboBox();
        positionComboBox.setOnAction(e -> {
            player.setPosition(positionComboBox.getValue().toString());
        });
        
        //CONTRACT SELECTION COMBOBOX
        contractLabel = new Label(CONTRACT_LABEL);
        contractComboBox = new ComboBox();
        contractComboBox.getItems().add("S1");
        contractComboBox.getItems().add("S2");
        contractComboBox.setOnAction(e -> {
            player.setContract(contractComboBox.getValue().toString());
            /*if(contractComboBox.getValue().toString().equals("<Enter Contract Type>")){
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                messageDialog.show(props.getProperty(WB_PropertyType.ILLEGAL_EDIT_PLAYER_MESSAGE));
            }*/
        });

        //SALARY SELECTION COMBOBOX
        salaryLabel = new Label(SALARY_LABEL);
        salaryTextField = new TextField();
        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals(""))
            player.setSalary(Integer.parseInt(newValue));
            
        });


        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            EditPlayerDialog.this.selection = sourceButton.getText();
            EditPlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(fantasyTeamsLabel, 0, 5, 1, 1);
        gridPane.add(fantasyTeamsComboBox, 1, 5, 1, 1);
        gridPane.add(positionLabel, 0, 6, 1, 1);
        gridPane.add(positionComboBox, 1, 6, 1, 1);
        gridPane.add(contractLabel, 0, 7, 1, 1);
        gridPane.add(contractComboBox, 1, 7, 1, 1);
        gridPane.add(salaryLabel, 0, 8, 1, 1);
        gridPane.add(salaryTextField, 1, 8, 1, 1);
        gridPane.add(completeButton, 0, 9, 1, 1);
        gridPane.add(cancelButton, 1, 9, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public Player getPlayer() { 
        return player;
    }
    
    public TextField getSalaryTextField(){
        return salaryTextField;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        fantasyTeamsComboBox.setValue(player.getFantasyTeamName());
        positionComboBox.setValue(player.getPosition());
        contractComboBox.setValue(player.getContract());
        salaryTextField.setText(Integer.toString(player.getSalary()));
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditPlayerDialog(Draft draft, Player playerToEdit){
        // SET THE DIALOG TITLE
        setTitle(HEADING_LABEL);
        //ADD IMAGES OF PLAYER, FLAG AND POSITIONS
        String playerImagePath = "file:" + PATH_IMAGES + PATH_PLAYERS_IMAGES + playerToEdit.getLastName() + playerToEdit.getFirstName() + ".jpg";
        ImageView playerImage = new ImageView(playerImagePath);
        gridPane.add(playerImage, 0, 1, 1, 4);
        
        String flagImagePath = "file:" + PATH_IMAGES + PATH_FLAGS_IMAGES + playerToEdit.getNationOfBirth() + ".png";
        ImageView flagImage = new ImageView(flagImagePath);
        gridPane.add(flagImage, 1, 1, 1, 4);
        //ADD NAME TO DIALOG
        Label nameLabel = new Label(playerToEdit.getFirstName() + " " + playerToEdit.getLastName());
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        gridPane.add(nameLabel, 1, 2);
        //ADD POSITIONS TO DIALOG
        Label positionsLabel = new Label();
        if(playerToEdit instanceof Hitter){
            positionsLabel.setText(((Hitter)playerToEdit).getQP());
        }
        else{
            positionsLabel.setText("P");
        }
        positionsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        gridPane.add(positionsLabel, 1, 4);
        
        //NOW LOAD IN THE DATA FIELDS
        if(playerToEdit instanceof Hitter){
            player = new Hitter();
        }
        else{
            player = new Pitcher();
        }
        //load fantasyTeams into the fantasyTeamsComboBox
        ObservableList<String> comboBoxItems = FXCollections.observableArrayList();
        comboBoxItems = copyStringArrayList(draft.getTeamNamesList(), comboBoxItems);
        fantasyTeamsComboBox.setItems(comboBoxItems);
        if(addFreeAgent){
            fantasyTeamsComboBox.getItems().add("Free Agent");
            addFreeAgent = false;
        }
        //load in positions into its combobox
        ObservableList<String> positions = FXCollections.observableArrayList();
        if(playerToEdit instanceof Hitter){
            positions.addAll(((Hitter)playerToEdit).getQP().split("_"));
            positionComboBox.setItems(positions);
        }
        else{
            positionComboBox.getItems().add("P");
        }
        // LOAD THE PLAYER INTO OUR LOCAL OBJECT
        playerToEdit.clone(player);
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();

    }
    
    private ObservableList<String> copyStringArrayList(ObservableList<String> src, ObservableList<String> dst){
        for(int i = 0; i < src.size(); i++){
            dst.add(i, src.get(i));
        }
        return dst;
    }
    
    private ObservableList<String> findEligiblePositions(Player playerToEdit, Draft draft){
        FantasyTeam selectedTeam = draft.getFantasyTeamMappings().get(fantasyTeamsComboBox.getValue().toString());
        ObservableList eligiblePositions = FXCollections.observableArrayList();
        if(playerToEdit instanceof Hitter){
            ObservableList positions = FXCollections.observableArrayList();
            positions.addAll(((Hitter)playerToEdit).getQP().split("_"));
            if(selectedTeam.getName().equals("Free Agent")){
                eligiblePositions.addAll(positions);
                return eligiblePositions;
            }
            for(int i = 0; i < positions.size(); i++){
                if(positions.get(i).equals("C")){
                    if(selectedTeam.getCountC() < 2){
                        eligiblePositions.add(positions.get(i));
                    }
                    
                }
                else if(positions.get(i).equals("1B")){
                    if(selectedTeam.getCount1B() < 1){
                        eligiblePositions.add(positions.get(i));
                    }
                }
                else if(positions.get(i).equals("CI")){
                    if(selectedTeam.getCountCI() < 1){
                        eligiblePositions.add(positions.get(i));
                    }
                }
                else if(positions.get(i).equals("3B")){
                    if(selectedTeam.getCount3B() < 1){
                        eligiblePositions.add(positions.get(i));
                    }
                }
                else if(positions.get(i).equals("2B")){
                    if(selectedTeam.getCount2B() < 1){
                        eligiblePositions.add(positions.get(i));
                    }
                }
                else if(positions.get(i).equals("MI")){
                    if(selectedTeam.getCountMI() < 1){
                        eligiblePositions.add(positions.get(i));
                    }
                }
                else if(positions.get(i).equals("SS")){
                    if(selectedTeam.getCountSS() < 1){
                        eligiblePositions.add(positions.get(i));
                    }
                }
                else if(positions.get(i).equals("OF")){
                    if(selectedTeam.getCountOF() < 1){
                        eligiblePositions.add(positions.get(i));
                    }
                }
                else if(positions.get(i).equals("U")){
                    if(selectedTeam.getCountU() < 1){
                        eligiblePositions.add(positions.get(i));
                    }
                }
            }
        }
        else{
            if((selectedTeam).getCountP() < 9){
                eligiblePositions.add("P");
            }
        }
        return eligiblePositions;
            
    }
        
}
    




