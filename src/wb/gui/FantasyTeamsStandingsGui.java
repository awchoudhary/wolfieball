package wb.gui;

import java.io.IOException;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static wb.WB_StartupConstants.PATH_CSS;
import wb.data.DraftDataManager;
import wb.WB_PropertyType;
import wb.data.FantasyTeam;
import static wb.gui.PlayersScreenGui.CLASS_RADIO_BUTTON_TOOLBAR;


/**
 * This class provides the Graphical User Interface for the fantasy teams standings
 * screen,
 * and displays the fantasy teams in the draft, along with their players
 * 
 * @author Awaes Choudhary
 */
public class FantasyTeamsStandingsGui {
    
    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES
    
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    
    //MANAGES ALL OF THE APPLICATIONS DATA
    DraftDataManager dataManager;
    
    //CONTAINS THE CONTENTS OF THE PAGE
    VBox fantasyTeamsStandingsBox;
    
    //PAGE HEADING
    Label fantasyTeamsStandingsLabel;
    
    //TABLE COMPONENTS
    TableView<FantasyTeam> fantasyStandingsTable;
    TableColumn nameColumn;
    TableColumn playersNeededColumn;
    TableColumn moneyLeftColumn;
    TableColumn moneyPerPlayerColumn;
    TableColumn RColumn;
    TableColumn HRColumn;
    TableColumn RBIColumn;
    TableColumn SBColumn;
    TableColumn BAColumn;
    TableColumn WColumn;
    TableColumn SVColumn;
    TableColumn KColumn;
    TableColumn ERAColumn;
    TableColumn WHIPColumn;
    TableColumn totalPointsColumn;
    
    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initSwitchScreenGui.
     *
     * @param initDataManager data manager for gui.**/
    public FantasyTeamsStandingsGui(DraftDataManager initDataManager){
        dataManager = initDataManager;
    }
    
    /**
     * Accessor method for the data manager.
     *
     * @return The DraftDataManager used by this UI.
     */
    public DraftDataManager getDataManager() {
        return dataManager;
    }
    
    /**
     * Accessor method for the fantasy teams pane.
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Pane getFantasyTeamsStandingsBox() {
        return fantasyTeamsStandingsBox;
    }
    
    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The CourseDataManager to be used by this UI.
     */
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }
    
    /**
     * This method fully initializes the user interface for use.
     *
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initFantasyTeamsStandingsGui(){
        //INIT THE FANTASY TEAMS STANDINGS BOX
        initFantasyTeamsStandingsBox();
        
    }
    
     /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/

    
    //INITIALIZES THE FANTASY TEAMS BOX
    private void initFantasyTeamsStandingsBox(){
        fantasyTeamsStandingsBox = new VBox();
        initChildLabel(fantasyTeamsStandingsBox, WB_PropertyType.FANTASY_TEAMS_STANDINGS_LABEL, CLASS_HEADING_LABEL);
        //INIT AND ADD TABLE TO BOX
        initTable();
        fantasyTeamsStandingsBox.getChildren().add(fantasyStandingsTable);
    }
    
    // INIT A LABEL AND PUT IT IN A TOOLBAR
    private Label initChildLabel(Pane container, WB_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }
    
    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(WB_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    //INIT THE TABLE
    private void initTable(){
        //INIT TABLE COMPONENTS
        fantasyStandingsTable = new TableView();
        nameColumn = new TableColumn("Team Name");
        playersNeededColumn = new TableColumn("Players Needed");
        moneyLeftColumn = new TableColumn("$ Left");
        moneyPerPlayerColumn = new TableColumn("$ PP");
        RColumn = new TableColumn("R");
        HRColumn = new TableColumn("HR");
        RBIColumn = new TableColumn("RBI");
        SBColumn = new TableColumn("SB");
        BAColumn = new TableColumn("BA");
        WColumn = new TableColumn("W");
        SVColumn = new TableColumn("SV");
        KColumn = new TableColumn("K");
        ERAColumn = new TableColumn("ERA");
        WHIPColumn = new TableColumn("WHIP");
        totalPointsColumn = new TableColumn("Total Points");
        
        //AND LINK THE COLUMNS TO THE DATA
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        playersNeededColumn.setCellValueFactory(new PropertyValueFactory<String, String>("playersNeeded"));
        moneyLeftColumn.setCellValueFactory(new PropertyValueFactory<String, String>("moneyLeft"));
        moneyPerPlayerColumn.setCellValueFactory(new PropertyValueFactory<String, String>("moneyPerPlayer"));
        RColumn.setCellValueFactory(new PropertyValueFactory<String, String>("R"));
        HRColumn.setCellValueFactory(new PropertyValueFactory<String, String>("HR"));
        RBIColumn.setCellValueFactory(new PropertyValueFactory<String, String>("RBI"));
        SBColumn.setCellValueFactory(new PropertyValueFactory<String, String>("SB"));
        BAColumn.setCellValueFactory(new PropertyValueFactory<String, String>("BA"));
        WColumn.setCellValueFactory(new PropertyValueFactory<String, String>("W"));
        SVColumn.setCellValueFactory(new PropertyValueFactory<String, String>("SV"));
        KColumn.setCellValueFactory(new PropertyValueFactory<String, String>("K"));
        ERAColumn.setCellValueFactory(new PropertyValueFactory<String, String>("ERA"));
        WHIPColumn.setCellValueFactory(new PropertyValueFactory<String, String>("WHIP"));
        totalPointsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("totalPoints"));
        
        //ADD COLUMNS TO TABLE
        fantasyStandingsTable.getColumns().add(nameColumn);
        fantasyStandingsTable.getColumns().add(playersNeededColumn);
        fantasyStandingsTable.getColumns().add(moneyLeftColumn);
        fantasyStandingsTable.getColumns().add(moneyPerPlayerColumn);
        fantasyStandingsTable.getColumns().add(RColumn);
        fantasyStandingsTable.getColumns().add(HRColumn);
        fantasyStandingsTable.getColumns().add(RBIColumn);
        fantasyStandingsTable.getColumns().add(SBColumn);
        fantasyStandingsTable.getColumns().add(BAColumn);
        fantasyStandingsTable.getColumns().add(WColumn);
        fantasyStandingsTable.getColumns().add(SVColumn);
        fantasyStandingsTable.getColumns().add(KColumn);
        fantasyStandingsTable.getColumns().add(ERAColumn);
        fantasyStandingsTable.getColumns().add(WHIPColumn);
        fantasyStandingsTable.getColumns().add(totalPointsColumn);
        fantasyStandingsTable.setItems(dataManager.getDraft().getTeams());
        fantasyStandingsTable.getStyleClass().add(CLASS_RADIO_BUTTON_TOOLBAR);
    }

    
    


}
