package wb.data;
import java.io.IOException;
import wb.file.DraftFileManager;
import static wb.WB_StartupConstants.JSON_FILE_PATH_HITTERS;
import static wb.WB_StartupConstants.JSON_FILE_PATH_PITCHERS;
import wb.gui.SwitchScreenGui;
/**
 * This class manages a Draft, which means it knows how to
 * reset one with default players.
 * 
 * @author Awaes Choudhary
 */
public class DraftDataManager {
    //THIS IS THE DRAFT BEING EDITED
    Draft draft;
    //THE FILE MANAGER TO LOAD THINGS INTO DRAFT
    DraftFileManager fileManager;
    //ALLOWS RELOAD OF DRAFT
    DraftDataView view;
    
    public DraftDataManager(DraftFileManager initDraftFileManager){
        fileManager = initDraftFileManager;
        draft = new Draft();
    }
    
    public Draft getDraft(){
        return draft;
    }
        
    
    /**
     * resets the draft and reloads all players into the unselected players list.
     * @param gui to activate the workspace
     */
    public void reset(SwitchScreenGui gui) throws IOException{
        draft.emptyPlayers();
        draft.emptyTeams();
        gui.getDraftNameTextField().setText("");
        fileManager.loadHitters(JSON_FILE_PATH_HITTERS, draft);
        fileManager.loadPitchers(JSON_FILE_PATH_PITCHERS, draft);
        gui.activateWorkspace();

    }
}
