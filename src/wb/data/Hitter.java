package wb.data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * An object to represent a hitter
 * @author Awaes
 */
public class Hitter extends Player {
    //HELPS IDENTIFY PLAYER TYPE WHEN LOADING FROM A JSON FILE
    String playerType = "Hitter";
    final StringProperty QP;
    final IntegerProperty R;
    final DoubleProperty H;
    final IntegerProperty HR;
    final IntegerProperty RBI;
    final IntegerProperty SB;
    final IntegerProperty AB;
    final DoubleProperty BA;
    
    //points for each category for the team
    int RPoint;
    int HRPoint;
    int RBIPoint;
    int SBPoint;
    int BAPoint;
    
    public Hitter(){
        QP = new SimpleStringProperty();
        R = new SimpleIntegerProperty();
        H = new SimpleDoubleProperty();
        HR = new SimpleIntegerProperty();
        RBI = new SimpleIntegerProperty();
        SB = new SimpleIntegerProperty();
        AB = new SimpleIntegerProperty();
        BA = new SimpleDoubleProperty();
    }
    
     public String getPlayerType(){
        return playerType;
     }
     public void setPlayerType(String newPlayerType){
         playerType = newPlayerType;
     }

    
    public String getQP(){
        return QP.get();
    }
    public void setQP(String newQP){
        QP.set(newQP);
    }
    public StringProperty QPProperty(){
        return QP;
    }
    
    public int getR(){
        return R.get();
    }
    public void setR(int newR){
        R.set(newR);
    }
    public IntegerProperty RProperty(){
        return R;
    }
    
    public int getHR(){
        return HR.get();
    }
    public void setHR(int newHR){
        HR.set(newHR);
    }
    public IntegerProperty HRProperty(){
        return HR;
    }
    
    
    public int getRBI(){
        return RBI.get();
    }
    public void setRBI(int newRBI){
        RBI.set(newRBI);
    }
    public IntegerProperty RBIProperty(){
        return RBI;
    }
    
    public int getSB(){
        return SB.get();
    }
    public void setSB(int newSB){
        SB.set(newSB);
    }
    public IntegerProperty SBProperty(){
        return SB;
    }
    
    public int getAB(){
        return AB.get();
    }
    public void setAB(int newAB){
        AB.set(newAB);
    }
    public IntegerProperty ABProperty(){
        return AB;
    }
    
    public double getH(){
        return H.get();
    }
    public void setH(double newH){
        H.set(newH);
    }
    public DoubleProperty HProperty(){
        return H;
    }
    
    public double getBA(){
        return BA.get();
    }
    public void setBA(double newBA){
        BA.set(round(newBA, 3));
    }
    public DoubleProperty BAProperty(){
        return BA;
    }
    
    public int getRPoint(){
        return RPoint;
    }
    public void setRPoint(int newPoint){
        RPoint = newPoint;
    }
    public int getHRPoint(){
        return HRPoint;
    }
    public void setHRPoint(int newPoint){
        HRPoint = newPoint;
    }
    public int getRBIPoint(){
        return RBIPoint;
    }
    public void setRBIPoint(int newPoint){
        RBIPoint = newPoint;
    }
    public int getSBPoint(){
        return SBPoint;
    }
    public void setSBPoint(int newPoint){
        SBPoint = newPoint;
    }
    public int getBAPoint(){
        return BAPoint;
    }
    public void setBAPoint(int newPoint){
        BAPoint = newPoint;
    }
    
    //CALCULATES AND SETS THE BA VALUE
    public void calculateAndSetBA(){
        if(AB.get() != 0){
            BA.set(H.get() / AB.get());
        }
        else{
            BA.set(0.0);
        }
    }
    
    public void clone(Hitter h){
        super.clone(h);
        h.setQP(getQP());
        h.setR(getR());
        h.setH(getH());
        h.setHR(getHR());
        h.setRBI(getRBI());
        h.setSB(getSB());
        h.setAB(getAB());
        h.setBA(getBA());
    }
    
    private double round(double value, int places) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
