package wb.data;

import java.time.LocalDate;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wb.comparator.*;

/**
 *An object to represent a Draft
 * @author Awaes
 */
public class Draft {
    //THIS ARRAY WILL HOLD ALL UNDRAFTED PLAYERS. 
    ObservableList<Player> Players;
    ObservableList<FantasyTeam> teams;
    ObservableList<String> teamNames;
    ObservableList<Player> draftedPlayers;
    
    public Draft(){
        Players = FXCollections.observableArrayList();
        teams = FXCollections.observableArrayList();
        teamNames = FXCollections.observableArrayList();
        draftedPlayers = FXCollections.observableArrayList();
    }
    
    public ObservableList<Player> getPlayersList(){
        return Players;
    }
    
    public void setPlayersList(ObservableList<Player> a){
        Players = a;
    }
    
    public ObservableList getTeamNamesList(){
        return teamNames;
    }
    
    public ObservableList<FantasyTeam> getTeams(){
        return teams;
    }
    
    public ObservableList getDraftedPlayers(){
        return draftedPlayers;
    }

    
    //ADDS A PLAYER TO THE UNSELECTED PLAYERS LIST
    public void addPlayer(Player playerToAdd){
        Players.add(playerToAdd);
    }
    
    //EMPTIES THE UNSELECTED PLAYERS LIST
    public void emptyPlayers(){
        Players.clear();
    }
    
    public void addTeam(FantasyTeam newTeam){
        teams.add(newTeam);
        teamNames.add(newTeam.getName());
    }
    
    public void removeTeam(FantasyTeam newTeam){
        teams.remove(newTeam);
        teamNames.remove(newTeam.getName());
    }
    
    public void emptyTeams(){
        teams.clear();
        teamNames.clear();
    }
    
    public boolean addToDraftedPlayers(Player p){
        if(p.getContract().equals("S2") || p.getContract().equals("X")){
            draftedPlayers.add(p);
            p.setDraftNumber(draftedPlayers.indexOf(p) + 1);
            return true;
        }
        else{
            return false;
        }
    }
    
    public void emptyDraftedPlayers(){
        draftedPlayers.clear();
    }
    
    public void removeFromDraftedPlayers(Player p){
        //SUBTRACT DRAFT NUMBERS OF PLAYERS AFTER THE PLAYER TO BE REMOVE BY 1
        int index = p.getDraftNumber();
        for(int i = index; i < draftedPlayers.size(); i++){
            draftedPlayers.get(i).setDraftNumber(draftedPlayers.get(i).getDraftNumber() - 1);
        }
        draftedPlayers.remove(getDraftedPlayersMappings().get(p.getFirstName()));
    }
    
    public HashMap<String, FantasyTeam> getFantasyTeamMappings() {
        // GET THE FANTASY TEAM FOR QUICK LOOKUP
        HashMap<String, FantasyTeam> fantasyTeams = new HashMap();
        for (FantasyTeam team : teams) {
            fantasyTeams.put(team.getName(), team);
        }
        return fantasyTeams;
    }
    
    public HashMap<String, Player> getDraftedPlayersMappings() {
        // GET THE FANTASY TEAM FOR QUICK LOOKUP
        HashMap<String, Player> draftedPlayersMap = new HashMap();
        for (Player p : draftedPlayers) {
            draftedPlayersMap.put(p.getFirstName(), p);
        }
        return draftedPlayersMap;
    }
    
    public void calculateAndAssignEstimatedValues(){
        ObservableList<Hitter> hitters = getHitters();
        ObservableList<Pitcher> pitchers = getPitchers();
        int totalHittersNeeded;
        int totalPitchersNeeded;
        int medianHittersSalary = 0;
        int medianPitchersSalary = 0;
        int totalRemainingMoney;
        rankHitters(hitters);
        rankPitchers(pitchers);
        totalHittersNeeded = getTotalHittersNeeded();
        totalPitchersNeeded = getTotalPitchersNeeded();
        totalRemainingMoney = getTotalRemainingMoney();
        if(totalHittersNeeded > 0)
        medianHittersSalary = totalRemainingMoney / (2 * totalHittersNeeded);
        if(totalPitchersNeeded > 0)
        medianPitchersSalary = totalRemainingMoney / (2 * totalPitchersNeeded);
        //calculate estimated values for hitters
        for(int i = 0; i < hitters.size(); i++){
            hitters.get(i).setEstimatedValue(medianHittersSalary * ((totalHittersNeeded * 2) / (i + 1)));
        }
        //calculate estimated value for pitchers
        for(int i = 0; i < pitchers.size(); i++){
            pitchers.get(i).setEstimatedValue(medianPitchersSalary * ((totalPitchersNeeded * 2) / (i + 1)));
        }
    }
    
    public void calculateAndAssignTotalPoints(){
        //sort base on R and assign points
        Collections.sort(teams, new FantasyTeamRComparator());
        assignPoints(teams, "R");
        Collections.sort(teams, new FantasyTeamHRComparator());
        assignPoints(teams, "HR");
        Collections.sort(teams, new FantasyTeamRBIComparator());
        assignPoints(teams, "RBI");
        Collections.sort(teams, new FantasyTeamSBComparator());
        assignPoints(teams, "SB");
        Collections.sort(teams, new FantasyTeamBAComparator());
        assignPoints(teams, "BA");
        Collections.sort(teams, new FantasyTeamWComparator());
        assignPoints(teams, "W");
        Collections.sort(teams, new FantasyTeamSVComparator());
        assignPoints(teams, "SV");
        Collections.sort(teams, new FantasyTeamKComparator());
        assignPoints(teams, "K");
        Collections.sort(teams, new FantasyTeamERAComparator());
        assignPoints(teams, "ERA");
        Collections.sort(teams, new FantasyTeamWHIPComparator());
        assignPoints(teams, "WHIP");
        calculateTotalPoints();
    }
    
    private void assignPoints(ObservableList<FantasyTeam> teams, String stat){
        for(int i = 1; i <= teams.size(); i++){
            if(stat.equals("R")){
                teams.get(i - 1).setRPoint(i);
            }
            if(stat.equals("HR")){
                teams.get(i - 1).setHRPoint(i);
            }
            if(stat.equals("RBI")){
                teams.get(i - 1).setRBIPoint(i);
            }
            if(stat.equals("SB")){
                teams.get(i - 1).setSBPoint(i);
            }
            if(stat.equals("BA")){
                teams.get(i - 1).setBAPoint(i);
            }
            if(stat.equals("W")){
                teams.get(i - 1).setWPoint(i);
            }
            if(stat.equals("SV")){
                teams.get(i - 1).setSVPoint(i);
            }
            if(stat.equals("K")){
                teams.get(i - 1).setKPoint(i);
            }
            if(stat.equals("ERA")){
                teams.get(i - 1).setERAPoint(i);
            }
            if(stat.equals("WHIP")){
                teams.get(i - 1).setWHIPPoint(i);
            }
        }
    }
    
    private void calculateTotalPoints(){
        for(int i = 0; i < teams.size(); i++){
            FantasyTeam team = teams.get(i);
            team.setTotalPoints(team.getRPoint() + team.getHRPoint() + team.getRBIPoint() + team.getSBPoint() + team.getBAPoint() + team.getWPoint() + team.getSVPoint() + team.getKPoint() + team.getERAPoint() + team.getWHIPPoint());
        }
    }
    
    private ObservableList<Hitter> getHitters(){
        ObservableList<Hitter> list = FXCollections.observableArrayList();
        for(int i = 0; i < Players.size(); i++){
            if(Players.get(i) instanceof Hitter){
                list.add((Hitter)Players.get(i));
            }
        }
        return list;
    }
    
    private ObservableList<Pitcher> getPitchers(){
        ObservableList<Pitcher> list = FXCollections.observableArrayList();
        for(int i = 0; i < Players.size(); i++){
            if(Players.get(i) instanceof Pitcher){
                list.add((Pitcher)Players.get(i));
            }
        }
        return list;
    }
    
    private void rankHitters(ObservableList<Hitter> list){
        Collections.sort(list, new HitterRComparator());
        assignPointsToHitters(list, "R");
        Collections.sort(list, new HitterHRComparator());
        assignPointsToHitters(list, "HR");
        Collections.sort(list, new HitterRBIComparator());
        assignPointsToHitters(list, "RBI");
        Collections.sort(list, new HitterSBComparator());
        assignPointsToHitters(list, "SB");
        Collections.sort(list, new HitterBAComparator());
        assignPointsToHitters(list, "BA");
        Collections.sort(list, new PlayerTotalPointsComparator());
    }
    
    private void rankPitchers(ObservableList<Pitcher> list){
        Collections.sort(list, new PitcherWComparator());
        assignPointsToPitchers(list, "W");
        Collections.sort(list, new PitcherSVComparator());
        assignPointsToPitchers(list, "SV");
        Collections.sort(list, new PitcherKComparator());
        assignPointsToPitchers(list, "K");
        Collections.sort(list, new PitcherERAComparator());
        assignPointsToPitchers(list, "ERA");
        Collections.sort(list, new PitcherWHIPComparator());
        assignPointsToPitchers(list, "WHIP");
        Collections.sort(list, new PlayerTotalPointsComparator());
    }
    
    private void assignPointsToHitters(ObservableList<Hitter> list, String stat){
        for(int i = 1; i <= list.size(); i++){
            if(stat.equals("R")){
                list.get(i - 1).setRPoint(i);
            }
            if(stat.equals("HR")){
                list.get(i - 1).setHRPoint(i);
            }
            if(stat.equals("RBI")){
                list.get(i - 1).setRBIPoint(i);
            }
            if(stat.equals("SB")){
                list.get(i - 1).setSBPoint(i);
            }
            if(stat.equals("BA")){
                list.get(i - 1).setBAPoint(i);
            }
            
        }
    }
    
    private void assignPointsToPitchers(ObservableList<Pitcher> list, String stat){
        for(int i = 1; i <= list.size(); i++){
            if(stat.equals("W")){
                list.get(i - 1).setWPoint(i);
            }
            if(stat.equals("SV")){
                list.get(i - 1).setSVPoint(i);
            }
            if(stat.equals("K")){
                list.get(i - 1).setKPoint(i);
            }
            if(stat.equals("ERA")){
                list.get(i - 1).setERAPoint(i);
            }
            if(stat.equals("WHIP")){
                list.get(i - 1).setWHIPPoint(i);
            }
            
        }
    }
    private int getTotalHittersNeeded(){
        int totalHittersNeeded = 0;
        for(int i = 0; i < teams.size(); i++){
            totalHittersNeeded += teams.get(i).getHittersNeeded();
        }
        return totalHittersNeeded;
    }
    
    private int getTotalPitchersNeeded(){
        int totalPitchersNeeded = 0;
        for(int i = 0; i < teams.size(); i++){
            totalPitchersNeeded += teams.get(i).getPitchersNeeded();
        }
        return totalPitchersNeeded;
    }
    
    private int getTotalRemainingMoney(){
        int totalRemainingMoney = 0;
        for(int i = 0; i < teams.size(); i++){
            totalRemainingMoney += teams.get(i).getMoneyLeft();
        }
        return totalRemainingMoney;
    }
}




