package wb.data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;

/**
 * An object to represent a pitcher
 * @author Awaes
 */
public class Pitcher extends Player{
    //HELPS IDENTIFY PLAYER TYPE WHEN LOADING FROM A JSON FILE
    String playerType = "Pitcher";
    final StringProperty role;
    final IntegerProperty W;
    final IntegerProperty SV;
    final IntegerProperty K;
    final IntegerProperty ER;
    final IntegerProperty H;
    final IntegerProperty BB;
    final DoubleProperty IP;
    final DoubleProperty ERA;
    final DoubleProperty WHIP;
    //points for each category for the team
    int WPoint;
    int SVPoint;
    int KPoint;
    int ERAPoint;
    int WHIPPoint;
    
    public Pitcher(){
        role = new SimpleStringProperty("P");
        W = new SimpleIntegerProperty();
        SV = new SimpleIntegerProperty();
        K = new SimpleIntegerProperty();
        ER = new SimpleIntegerProperty();
        H = new SimpleIntegerProperty();
        BB = new SimpleIntegerProperty();
        IP = new SimpleDoubleProperty();
        ERA = new SimpleDoubleProperty();
        WHIP = new SimpleDoubleProperty();
    }
    
    public String getPlayerType(){
        return playerType;
    }
    public void setPlayerType(String newPlayerType){
         playerType = newPlayerType;
     }

    
    public String getRole(){
        return role.get();
    }
    public void setRole(String newRole){
        role.set(newRole);
    }
    public StringProperty QPProperty(){
        return role;
    }
    
    public int getW(){
        return W.get();
    }
    public void setW(int newW){
        W.set(newW);
    }
    public IntegerProperty RProperty(){
        return W;
    }
    
    public int getSV(){
        return SV.get();
    }
    public void setSV(int newSV){
        SV.set(newSV);
    }
    public IntegerProperty HRProperty(){
        return SV;
    }
    
    public int getK(){
        return K.get();
    }
    public void setK(int newK){
        K.set(newK);
    }
    public IntegerProperty RBIProperty(){
        return K;
    }
    
    public double getERA(){
        return ERA.get();
    }
    public void setERA(double newERA){
        ERA.set(newERA);
    }
    public DoubleProperty SBProperty(){
        return ERA;
    }
    
    public double getWHIP(){
        return WHIP.get();
    }
    public void setWHIP(double newWHIP){
        WHIP.set(newWHIP);
    }
    public DoubleProperty BAProperty(){
        return WHIP;
    }
    
    public int getER(){
        return ER.get();
    }
    public void setER(int newER){
        ER.set(newER);
    }
    public IntegerProperty ERProperty(){
        return ER;
    }
    
    public int getH(){
        return H.get();
    }
    public void setH(int newH){
        H.set(newH);
    }
    public IntegerProperty HProperty(){
        return H;
    }
    
    public int getBB(){
        return BB.get();
    }
    public void setBB(int newBB){
        BB.set(newBB);
    }
    public IntegerProperty BBProperty(){
        return BB;
    }
    
    public double getIP(){
        return IP.get();
    }
    public void setIP(double newIP){
        IP.set(newIP);
    }
    public DoubleProperty IPProperty(){
        return IP;
    }
    public int getWPoint(){
        return WPoint;
    }
    public void setWPoint(int newPoint){
        WPoint = newPoint;
    }
    public int getSVPoint(){
        return SVPoint;
    }
    public void setSVPoint(int newPoint){
        SVPoint = newPoint;
    }
    public int getKPoint(){
        return KPoint;
    }
    public void setKPoint(int newPoint){
        KPoint = newPoint;
    }
    public int getERAPoint(){
        return ERAPoint;
    }
    public void setERAPoint(int newPoint){
        ERAPoint = newPoint;
    }
    public int getWHIPPoint(){
        return WHIPPoint;
    }
    public void setWHIPPoint(int newPoint){
        WHIPPoint = newPoint;
    }
    
    //CALCULATES AND SETS THE ERA VALUE
    public void calculateAndSetERA(){
        if(IP.get() != 0){
            ERA.set(round((ER.get() * 9) / IP.get(), 2));
        }
        else{
            ERA.set(0.0);
        }
    }
    
    //CALCULATES AND SETS THE WHIP VALUE
    public void calculateAndSetWHIP(){
        if(IP.get() != 0){
            WHIP.set(round((BB.get() + H.get()) / IP.get(), 2));
        }
        else{
            WHIP.set(0.0);
        }
    }
    
    public void clone(Pitcher p){
        super.clone(p);
        p.setRole(getRole());
        p.setW(getW());
        p.setSV(getSV());
        p.setK(getK());
        p.setER(getER());
        p.setH(getH());
        p.setBB(getBB());
        p.setIP(getIP());
        p.setERA(getERA());
        p.setWHIP(getWHIP());
    }
    
    private double round(double value, int places) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
}
