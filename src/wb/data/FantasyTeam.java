package wb.data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * An object to represent a fantasy team
 * @author Awaes
 */
public class FantasyTeam {
    StringProperty name;
    StringProperty ownerName;
    IntegerProperty playersNeeded;
    DoubleProperty moneyLeft;
    DoubleProperty moneyPerPlayer;
    IntegerProperty R;
    IntegerProperty HR;
    IntegerProperty RBI;
    IntegerProperty SB;
    DoubleProperty BA;
    IntegerProperty W;
    IntegerProperty SV;
    IntegerProperty K;
    DoubleProperty ERA;
    DoubleProperty WHIP;
    IntegerProperty totalPoints;
    
    //points for each category for the team
    int RPoint;
    int HRPoint;
    int RBIPoint;
    int SBPoint;
    int BAPoint;
    int WPoint;
    int SVPoint;
    int KPoint;
    int ERAPoint;
    int WHIPPoint;
    
    int counter = 0;
    
    
    ObservableList<Player> startingLineUp;
    ObservableList<Player> visibleStartingLineUp;
    ObservableList<Player> taxiSquad;
    //counters to determine players of a certain position in the team
    int countC = 0;
    int count1B = 0;
    int countCI = 0;
    int count3B = 0;
    int count2B = 0;
    int countMI = 0;
    int countSS = 0;
    int countU = 0;
    int countOF = 0;
    int countP = 0;
    //indexes to add the starting lineup
    int indexC = 0;
    int index1B = 2;
    int indexCI = 3;
    int index3B = 4;
    int index2B = 5;
    int indexMI = 6;
    int indexSS = 7;
    int indexU = 13;
    int indexOF = 8;
    int indexP = 14;

    
    
    public FantasyTeam(){
        startingLineUp = FXCollections.observableArrayList();
        visibleStartingLineUp = FXCollections.observableArrayList();
        taxiSquad = FXCollections.observableArrayList();
        name = new SimpleStringProperty();
        ownerName = new SimpleStringProperty();
        playersNeeded = new SimpleIntegerProperty(23 - visibleStartingLineUp.size());
        moneyLeft = new SimpleDoubleProperty(260.0 - getTotalSalaries());
        moneyPerPlayer = new SimpleDoubleProperty(round(moneyLeft.get()/playersNeeded.get(), 2));
        R = new SimpleIntegerProperty(getAggregateR());
        HR = new SimpleIntegerProperty(getAggregateHR());
        RBI = new SimpleIntegerProperty(getAggregateRBI());
        SB = new SimpleIntegerProperty(getAggregateSB());
        BA = new SimpleDoubleProperty(getAggregateBA());
        W = new SimpleIntegerProperty(getAggregateW());
        SV = new SimpleIntegerProperty(getAggregateSV());
        K = new SimpleIntegerProperty(getAggregateK());
        ERA = new SimpleDoubleProperty(getAggregateERA());
        WHIP = new SimpleDoubleProperty(getAggregateWHIP());
        totalPoints = new SimpleIntegerProperty();
        
        //fill up the starting line up array so that players can be added to 
        //it in specific indexes later on
        for(int i = 0; i < 24; i++){
            Player p = new Player();
            startingLineUp.add(p);
        }
    }
    /********************************************************************************************************************
     * GETTERS AND SETTERS AND PROPERTY METHODS
     *******************************************************************************************************************/
     
    public int getCountC(){
        return countC;
    }
    public int getCount1B(){
        return count1B;
    }
    public int getCountCI(){
        return countCI;
    }
    public int getCount3B(){
        return count3B;
    }
    public int getCount2B(){
        return count2B;
    }
    public int getCountMI(){
        return countMI;
    }
    public int getCountSS(){
        return countSS;
    }
    public int getCountU(){
        return countU;
    }
    public int getCountOF(){
        return countOF;
    }
    public int getCountP(){
        return countP;
    }
    
    public int getRPoint(){
        return RPoint;
    }
    public void setRPoint(int newPoint){
        RPoint = newPoint;
    }
    public int getHRPoint(){
        return HRPoint;
    }
    public void setHRPoint(int newPoint){
        HRPoint = newPoint;
    }
    public int getRBIPoint(){
        return RBIPoint;
    }
    public void setRBIPoint(int newPoint){
        RBIPoint = newPoint;
    }
    public int getSVPoint(){
        return SVPoint;
    }
    public void setSVPoint(int newPoint){
        SVPoint = newPoint;
    }
    public int getBAPoint(){
        return BAPoint;
    }
    public void setBAPoint(int newPoint){
        BAPoint = newPoint;
    }
    public int getWPoint(){
        return WPoint;
    }
    public void setWPoint(int newPoint){
        WPoint = newPoint;
    }
    public int getSBPoint(){
        return SBPoint;
    }
    public void setSBPoint(int newPoint){
        SBPoint = newPoint;
    }
    public int getKPoint(){
        return KPoint;
    }
    public void setKPoint(int newPoint){
        KPoint = newPoint;
    }
    public int getERAPoint(){
        return ERAPoint;
    }
    public void setERAPoint(int newPoint){
        ERAPoint = newPoint;
    }
    public int getWHIPPoint(){
        return WHIPPoint;
    }
    public void setWHIPPoint(int newPoint){
        WHIPPoint = newPoint;
    }
    
    public String getName(){
        return name.get();
    }
    public void setName(String newName){
        name.set(newName);
    }
    public StringProperty nameProperty(){
        return name;
    }
    
    public String getOwnerName(){
        return ownerName.get();
    }
    public void setOwnerName(String newName){
        ownerName.set(newName);
    }
    public StringProperty ownerNameProperty(){
        return ownerName;
    }
    
    public int getPlayersNeeded(){
        return playersNeeded.get();
    }
    public void setPlayersNeeded(int newPlayersNeeded){
        playersNeeded.set(newPlayersNeeded);
    }
    public IntegerProperty playersNeededProperty(){
        return playersNeeded;
    }
    
    public double getMoneyLeft(){
        return moneyLeft.get();
    }
    public void setMoneyLeft(int newMoneyLeft){
        moneyLeft.set(newMoneyLeft);
    }
    public DoubleProperty moneyLeftProperty(){
        return moneyLeft;
    }
    
    public double getMoneyPerPlayer(){
        return moneyPerPlayer.get();
    }
    public void setMoneyPerPlayer(double newMoneyPerPlayer){
        moneyPerPlayer.set(newMoneyPerPlayer);
    }
    public DoubleProperty moneyPerPlayerProperty(){
        return moneyPerPlayer;
    }
    
    public int getR(){
        return R.get();
    }
    public void setR(int newR){
        R.set(newR);
    }
    public IntegerProperty RProperty(){
        return R;
    }
    
    public int getHR(){
        return HR.get();
    }
    public void setHR(int newHR){
        HR.set(newHR);
    }
    public IntegerProperty HRProperty(){
        return HR;
    }
    
    public int getRBI(){
        return RBI.get();
    }
    public void setRBI(int newRBI){
        RBI.set(newRBI);
    }
    public IntegerProperty RBIProperty(){
        return RBI;
    }
    
    public int getSB(){
        return SB.get();
    }
    public void setSB(int newSB){
        SB.set(newSB);
    }
    public IntegerProperty SBProperty(){
        return SB;
    }
    
    public double getBA(){
        return BA.get();
    }
    public void setBA(double newBA){
        BA.set(newBA);
    }
    public DoubleProperty BAProperty(){
        return BA;
    }
    public int getW(){
        return W.get();
    }
    public IntegerProperty WProperty(){
        return W;
    }
    public int getSV(){
        return SV.get();
    }
    public IntegerProperty SVProperty(){
        return SV;
    }
    public int getK(){
        return K.get();
    }
    public IntegerProperty KProperty(){
        return K;
    }
    public double getERA(){
        return ERA.get();
    }
    public DoubleProperty ERAProperty(){
        return ERA;
    }
    public double getWHIP(){
        return WHIP.get();
    }
    public DoubleProperty WHIPProperty(){
        return WHIP;
    }
    
    public int getTotalPoints(){
        return totalPoints.get();
    }
    public void setTotalPoints(int newTotalPoints){
        totalPoints.set(newTotalPoints);
    }
    public IntegerProperty totalPointsProperty(){
        return totalPoints;
    }
    
    public ObservableList<Player> getStartingLineUp(){
        return startingLineUp;
    }

    
    public ObservableList<Player> getVisibleStartingLineUp(){
        return visibleStartingLineUp;
    }
    
    public ObservableList<Player> getTaxiSquad(){
        return taxiSquad;
    }
    
    /*******************************************************************************************************************/
    
    public void addToVisibleStartingLineUp(Player p){
        if(visibleStartingLineUp.size() < 23){
            System.out.println(visibleStartingLineUp.size());
            addPlayerToStartingLineUp(p);
            copyPlayersArray();
            updateTeamStats();
            counter++;
        }
        else{
            addToTaxiSquad(p);
        }
    }
    
    public void addToVisibleStartingLineUpForAutomatedDraft(Player p){
        if(counter < 23){
            //System.out.println(counter);
            addPlayerToStartingLineUp(p);
            copyPlayersArray();
            updateTeamStats();
            counter++;
        }
        else{
            addToTaxiSquad(p);
        }
    }
    
    public void removeFromVisibleStartingLineUp(Player p){
        p = startingLineUp.get(getIndexOfPlayer(startingLineUp, p));
        startingLineUp.remove(p);
        copyPlayersArray();
        updateTeamStats();
    }
    
    

 /***********************************************************************************************************************
        Private Helper Methods
 ************************************************************************************************************************/
    
    //adds a player to the starting lineup byreferencing the positionsReferenceArray
    //return true or false depending on whether a player was added
    private boolean addPlayerToStartingLineUp(Player p){
        String position;
        //initialize position to the position of the player
        position = p.getPosition();
        //check to see if there is room for a player of
        //the player argument's position and add it accoring to its position in the list
        if(position.equals("C")){
            if(countC < 2){
                startingLineUp.set(indexC, p);
                indexC++;
                countC++;
                return true;
            }
            
        }
        if(position.equals("1B")){
            if(count1B == 0){
                startingLineUp.set(index1B, p);
                count1B++;
                return true;
            }
            
        }
        if(position.equals("CI")){
            if(countCI == 0){
                startingLineUp.set(indexCI, p);
                countCI++;
                return true;
            }
            
        }
        if(position.equals("3B")){
            if(count3B == 0){
                startingLineUp.set(index3B, p);
                count3B++;
                return true;
            }
            
        }
        if(position.equals("2B")){
            if(count2B == 0){
                startingLineUp.set(index2B, p);
                count2B++;
                return true;
            }
            
        }
        if(position.equals("MI")){
            if(countMI == 0){
                startingLineUp.set(indexMI, p);
                countMI++;
                return true;
            }
        }
        if(position.equals("SS")){
            if(countSS == 0){
                startingLineUp.set(indexSS, p);
                countSS++;
                return true;
            }
        }
        if(position.equals("OF")){
            if(countOF < 5){
                startingLineUp.set(indexOF, p);
                indexOF++;
                countOF++;
                return true;
            }
            
        }

        if(position.equals("U")){
            if(countU == 0){
                startingLineUp.set(indexU, p);
                countU++;
                return true;
            }
            
        }
        if(position.equals("P")){
            if(countP < 9){
                startingLineUp.set(indexP, p);
                indexP++;
                countP++;
                return true;
            }
            
        }
        
        return false;
        
    }
    
        
        
    //copies the startingLineUp array into the visible array 
    //excluding the filler players
    private void copyPlayersArray(){
        visibleStartingLineUp.clear();
        for(int i = 0; i < startingLineUp.size(); i++){
            if(startingLineUp.get(i) instanceof Hitter){
                Hitter h = new Hitter();
                ((Hitter)startingLineUp.get(i)).clone(h);
                visibleStartingLineUp.add(h);
            }
            else if(startingLineUp.get(i) instanceof Pitcher){
                 Pitcher p = new Pitcher();
                ((Pitcher)startingLineUp.get(i)).clone(p);
                visibleStartingLineUp.add(p);
            }

        }
    }

    
    private void addToTaxiSquad(Player p){
        if(taxiSquad.size() < 9){
            p.setContract("X");
            taxiSquad.add(p);
        }
    }
    
    private int getIndexOfPlayer(ObservableList<Player> list, Player p){
        for(int i = 0; i < list.size(); i++){
            if(p.equals(list.get(i))){
                return i;
            }
        }
        return 0;
    }
    
    private int getTotalSalaries(){
        int totalSalaries = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            totalSalaries += visibleStartingLineUp.get(i).getSalary();
        }
        return totalSalaries;
    }
    
    private int getAggregateR(){
        int aggregateR = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Hitter){
                aggregateR += ((Hitter)visibleStartingLineUp.get(i)).getR();
            }
        }
        return aggregateR;
    }
    private int getAggregateHR(){
        int aggregateHR = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Hitter){
                aggregateHR += ((Hitter)visibleStartingLineUp.get(i)).getHR();
            }
        }
        return aggregateHR;
    }
    private int getAggregateRBI(){
        int aggregateRBI = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Hitter){
                aggregateRBI += ((Hitter)visibleStartingLineUp.get(i)).getRBI();
            }
        }
        return aggregateRBI;
    }
    private int getAggregateSB(){
        int aggregateSB = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Hitter){
                aggregateSB += ((Hitter)visibleStartingLineUp.get(i)).getSB();
            }
        }
        return aggregateSB;
    }
    private double getAggregateBA(){
        //get the aggregate hits
        double aggregateBA = 0;
        double aggregateH = 0;
        double aggregateAB = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Hitter){
                aggregateH += ((Hitter)visibleStartingLineUp.get(i)).getH();
                aggregateAB += ((Hitter)visibleStartingLineUp.get(i)).getAB();
            }
        }
        if(aggregateH == 0){
            aggregateBA = 0;
        }
        else{
            //then calculate aggregate BA
            aggregateBA = aggregateH / aggregateAB;
        }
        return aggregateBA;
    }
    private int getAggregateW(){
        int aggregateW = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Pitcher){
                aggregateW += ((Pitcher)visibleStartingLineUp.get(i)).getW();
            }
        }
        return aggregateW;
    }
    private int getAggregateSV(){
        int aggregateSV = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Pitcher){
                aggregateSV += ((Pitcher)visibleStartingLineUp.get(i)).getSV();
            }
        }
        return aggregateSV;
    }
    private int getAggregateK(){
        int aggregateK = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Pitcher){
                aggregateK += ((Pitcher)visibleStartingLineUp.get(i)).getK();
            }
        }
        return aggregateK;
    }
    private double getAggregateERA(){
        double aggregateERA = 0;
        double aggregateER = 0;
        double aggregateIP = 0;
        //get aggregate ER and IP
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Pitcher){
                aggregateER += ((Pitcher)visibleStartingLineUp.get(i)).getER();
                aggregateIP += ((Pitcher)visibleStartingLineUp.get(i)).getIP();
            }
        }
        if(aggregateER == 0){
            aggregateERA = 0;
        }
        else{
            //calculate aggregate ERA
            aggregateERA = (aggregateER * 9) / aggregateIP;
        }
        return aggregateERA;
    }
    private double getAggregateWHIP(){
        double aggregateWHIP = 0;
        double aggregateBB = 0;
        double aggregateH = 0;
        double aggregateIP = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Pitcher){
                aggregateBB += ((Pitcher)visibleStartingLineUp.get(i)).getBB();
                aggregateH += ((Pitcher)visibleStartingLineUp.get(i)).getH();
                aggregateIP += ((Pitcher)visibleStartingLineUp.get(i)).getIP();
            }
        }
        if((aggregateBB + aggregateH) == 0){
            aggregateWHIP = 0;
        }
        else{
            //then calculate aggregate WHIP
            aggregateWHIP = (aggregateBB + aggregateH) / aggregateIP;
        }
        return aggregateWHIP;
    }
    
    private void updateTeamStats(){
        playersNeeded.set(23 - visibleStartingLineUp.size());
        moneyLeft.set(260.0 - getTotalSalaries());
        if(playersNeeded.get() == 0){
            moneyPerPlayer.set(-1);
        }
        else{
            moneyPerPlayer.set(round(moneyLeft.get()/playersNeeded.get(), 2));
        }
        R.set(getAggregateR());
        HR.set(getAggregateHR());
        RBI.set(getAggregateRBI());
        SB.set(getAggregateSB());
        BA.set(round(getAggregateBA(), 3));
        W.set(getAggregateW());
        SV.set(getAggregateSV());
        K.set(getAggregateK());
        ERA.set(round(getAggregateERA(), 2));
        WHIP.set(round(getAggregateWHIP(), 2));
    }
    
    private double round(double value, int places) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    public int getHittersNeeded(){
        int hitterCount = 0;
        int hittersNeeded = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Hitter){
                hitterCount++;
            }
        }
        hittersNeeded = 14 - hitterCount;
        return hittersNeeded;
    }
    
    public int getPitchersNeeded(){
        int pitcherCount = 0;
        int pitchersNeeded = 0;
        for(int i = 0; i < visibleStartingLineUp.size(); i++){
            if(visibleStartingLineUp.get(i) instanceof Pitcher){
                pitcherCount++;
            }
        }
        pitchersNeeded = 9 - pitcherCount;
        return pitchersNeeded;
    }
    
    public int getTaxiPlayersNeeded(){
        return 9 - taxiSquad.size();
    }

}
