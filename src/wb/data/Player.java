package wb.data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * An object to represent a player
 * @author Awaes
 */
public class Player {
    //HELPS IDENTIFY PLAYER TYPE WHEN LOADING FROM A JSON FILE
    String playerType = "Player";
    final StringProperty position;
    final StringProperty firstName;
    final StringProperty lastName;
    final StringProperty fantasyTeamName;
    final StringProperty fantasyTeamOwner;
    final StringProperty teamMLB;
    final StringProperty contract;
    final IntegerProperty salary;
    final StringProperty notes;
    final StringProperty yearOfBirth;
    final StringProperty nationOfBirth;
    final IntegerProperty estimatedValue;
    final IntegerProperty draftNumber;
    final IntegerProperty totalPoints;
    
    
    public Player(){
        position = new SimpleStringProperty("<Enter Position>");
        firstName = new SimpleStringProperty("<Enter Name>");
        lastName = new SimpleStringProperty("<Enter Last Name>");
        fantasyTeamName = new SimpleStringProperty("Free Agent");
        fantasyTeamOwner = new SimpleStringProperty("<Enter Owner>");
        teamMLB = new SimpleStringProperty("<Enter MLB Team>");
        contract = new SimpleStringProperty("<Enter Contact Type>");
        salary = new SimpleIntegerProperty();
        notes = new SimpleStringProperty("<Notes>");
        yearOfBirth = new SimpleStringProperty("<Enter Year>");
        nationOfBirth = new SimpleStringProperty("<Enter Nation>");
        estimatedValue = new SimpleIntegerProperty();
        draftNumber = new SimpleIntegerProperty();
        totalPoints = new SimpleIntegerProperty();
    }
    
    public String getPlayerType(){
        return playerType;
    }
    
    public String getPosition(){
        return position.get();
    }
    public void setPosition(String newPosition){
        position.set(newPosition);
    }
    public StringProperty positionProperty(){
        return position;
    }
    
    
    public String getLastName(){
        return lastName.get();
    }
    public void setLastName(String newLastName){
        lastName.set(newLastName);
    }
    public StringProperty lastNameProperty(){
        return lastName;
    }
    
    public String getFirstName(){
        return firstName.get();
    }
    public void setFirstName(String newFirstName){
        firstName.set(newFirstName);
    }
    public StringProperty firstNameProperty(){
        return firstName;
    }
    
    public String getFantasyTeamName(){
        return fantasyTeamName.get();
    }
    public void setFantasyTeamName(String newTeam){
        fantasyTeamName.set(newTeam);
    }
    public StringProperty fantasyTeamNameProperty(){
        return fantasyTeamName;
    }
    
    public String getFantasyTeamOwner(){
        return fantasyTeamOwner.get();
    }
    public void setFantasyTeamOwner(String newOwner){
        fantasyTeamOwner.set(newOwner);
    }
    public StringProperty fantasyTeamOwnerProperty(){
        return fantasyTeamOwner;
    }

    
    public String getTeamMLB(){
        return teamMLB.get();
    }
    public void setTeamMLB(String newTeamMLB){
        teamMLB.set(newTeamMLB);
    }
    public StringProperty teamMLBProperty(){
        return teamMLB;
    }
    
    public String getContract(){
        return contract.get();
    }
    public void setContract(String newContract){
        contract.set(newContract);
    }
    public StringProperty contractProperty(){
        return contract;
    }
    
    public int getSalary(){
        return salary.get();
    }
    public void setSalary(int newSalary){
        salary.set(newSalary);
    }
    public IntegerProperty salaryProperty(){
        return salary;
    }
    
    public String getNotes(){
        return notes.get();
    }
    public void setNotes(String newNotes){
        notes.set(newNotes);
    }
    public StringProperty notesProperty(){
        return notes;
    }
    
    public String getYearOfBirth(){
        return yearOfBirth.get();
    }
    public void setYearOfBirth(String newYearOfBirth){
        yearOfBirth.set(newYearOfBirth);
    }
    public StringProperty yearOfBirthProperty(){
        return yearOfBirth;
    }
    
    public String getNationOfBirth(){
        return nationOfBirth.get();
    }
    public void setNationOfBirth(String newNationOfBirth){
        nationOfBirth.set(newNationOfBirth);
    }
    public StringProperty nationOfBirthProperty(){
        return nationOfBirth;
    }
    
    public int getEstimatedValue(){
        return estimatedValue.get();
    }
    public void setEstimatedValue(int newEstimatedValue){
        estimatedValue.set(newEstimatedValue);
    }
    public IntegerProperty estimatedValueProperty(){
        return estimatedValue;
    }
    
    public int getTotalPoints(){
        return totalPoints.get();
    }
    public void setTotalPoints(int newTotalPoints){
        totalPoints.set(newTotalPoints);
    }
   
    
    public int getDraftNumber(){
        return draftNumber.get();
    }
    public void setDraftNumber(int newDraftNumber){
        draftNumber.set(newDraftNumber);
    }
    public IntegerProperty draftNumberProperty(){
        return draftNumber;
    }
    
    //method to clone player
    public void clone(Player p){
        p.setDraftNumber(getDraftNumber());
        p.setPosition(getPosition());
        p.setFirstName(getFirstName());
        p.setLastName(getLastName());
        p.setFantasyTeamName(getFantasyTeamName());
        p.setFantasyTeamOwner(getFantasyTeamOwner());
        p.setTeamMLB(getTeamMLB());
        p.setContract(getContract());
        p.setSalary(getSalary());
        p.setNotes(getNotes());
        p.setYearOfBirth(getYearOfBirth());
        p.setNationOfBirth(getNationOfBirth());
        p.setEstimatedValue(getEstimatedValue());
    }
    
    public boolean equals(Player p){
        if(!getFirstName().equals(p.getFirstName())){
            return false;
        }
        if(!getLastName().equals(p.getLastName())){
            return false;
        }
        if(!getTeamMLB().equals(p.getTeamMLB())){
            return false;
        }
        if(!getYearOfBirth().equals(p.getYearOfBirth())){
            return false;
        }
        if(!getNationOfBirth().equals(p.getNationOfBirth())){
            return false;
        }
        return true;
    }
}
